package com.apps.lifesavi.blacktube.operation;


import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.BaseTubeResponse;
import com.apps.lifesavi.blacktube.utils.TubePreference;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.apps.lifesavi.blacktube.constant.Constants.QueryConstants.KEY;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.YOUTUBE_DEVELOPER_KEY;
import static com.apps.lifesavi.blacktube.constant.Constants.UrlConstants.GET_REGION_LIST;

public class GetRegionListOperation extends BaseWebOperation<BaseTubeResponse> {

    @Override
    public void onSuccess(BaseTubeResponse response) {
        mOnOperationListener.onOperationListenerSuccess(response);
    }

    @Override
    public void onError(NetworkErrorThrowable t) {
        mOnOperationListener.onOperationListenerError(t);
    }

    public interface GetRegionOperation {
        @GET(GET_REGION_LIST)
        Call<BaseTubeResponse> getRegionList(@Query(KEY) String key);
    }

    public GetRegionListOperation(OnOperationListener<BaseTubeResponse> listener) {
        mOnOperationListener = listener;
        getRetrofitInstance().create(GetRegionOperation.class).getRegionList(FirebaseRemoteConfig.getInstance().getString(YOUTUBE_DEVELOPER_KEY)).enqueue(this);
        TubePreference.getInstance().incrementQuotaCountBy(1);
    }
}
