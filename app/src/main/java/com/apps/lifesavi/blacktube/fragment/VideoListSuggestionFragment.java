package com.apps.lifesavi.blacktube.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apps.lifesavi.blacktube.BuildConfig;
import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.adapter.BaseTubeContentListAdapter;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.model.VideoRequest;
import com.apps.lifesavi.blacktube.operation.manager.TubeManager;
import com.apps.lifesavi.blacktube.utils.TubeDb;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.List;

import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.CATEGORY_ID;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.CURRENT_VIDEO_ID;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.HAS_NAVIGATION;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.ITEM_TYPE;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.SUGGESTION_VIDEO_TYPE;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.TITLE;

public class VideoListSuggestionFragment extends android.app.Fragment implements RewardedVideoAdListener {

    public BaseTubeContentListAdapter mContentListRecyclerAdapter;
    private OnItemClickListener<TubeItem> mOnVideoItemClickListener;

    private VideoRequest mVideoRequest;
    private ProgressBar mProgressBar;

    private int lastPlayingPosition;
    private boolean isSaveRequired;
    private LinearLayoutManager mLinearLayoutManager;

    //Ad
    private RewardedVideoAd mAd;
    private boolean isAdWatched;
    private TubeItem mPendingTubeItem;
    private int mPendingTubeItemPosition;
    private boolean isAdsEnabled = FirebaseRemoteConfig.getInstance().getBoolean(Constants.RemoteConfigConstant.IS_VIDEO_ADS_ENABLED);

    public void setOnVideoItemClickListener(OnItemClickListener<TubeItem> listener) {
        mOnVideoItemClickListener = listener;
    }

    public static VideoListSuggestionFragment newInstance(VideoRequest videoRequest) {
        VideoListSuggestionFragment fragment = new VideoListSuggestionFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_ID, videoRequest.categoryId);
        bundle.putString(TITLE, videoRequest.title);
        bundle.putString(CURRENT_VIDEO_ID, videoRequest.currentVideoId);
        bundle.putInt(SUGGESTION_VIDEO_TYPE, videoRequest.suggestionVideoType);
        bundle.putInt(ITEM_TYPE, videoRequest.itemType);
        bundle.putBoolean(HAS_NAVIGATION, videoRequest.hasNavigation);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_list, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        mProgressBar.setVisibility(View.VISIBLE);

        mAd = MobileAds.getRewardedVideoAdInstance(mProgressBar.getContext());
        mAd.setRewardedVideoAdListener(this);

        //Setting Data
        Bundle bundle = getArguments();
        mVideoRequest = new VideoRequest();
        mVideoRequest.title = bundle.getString(TITLE);
        mVideoRequest.categoryId = bundle.getString(CATEGORY_ID);
        mVideoRequest.currentVideoId = bundle.getString(CURRENT_VIDEO_ID);
        mVideoRequest.suggestionVideoType = bundle.getInt(SUGGESTION_VIDEO_TYPE);
        mVideoRequest.hasNavigation = bundle.getBoolean(HAS_NAVIGATION);
        mVideoRequest.itemType = bundle.getInt(ITEM_TYPE);

        if (isAdded() && isAdsEnabled) {
            loadRewardedVideoAd();
        }

        mContentListRecyclerAdapter = BaseTubeContentListAdapter.getInstance(mVideoRequest.itemType);
        mContentListRecyclerAdapter.setTubeItemListListener(new BaseTubeContentListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TubeItem item, final int position) {

                if (item.lockStatus != TubeItem.LOCK_STATUS_LOCKED && mOnVideoItemClickListener != null) {
                    lastPlayingPosition = position;
                    mOnVideoItemClickListener.onItemClick(item);
                    return;
                }

                if (item.lockStatus == TubeItem.LOCK_STATUS_LOCKED) {
                    if (isAdsEnabled && mAd != null && mAd.isLoaded()) {
                        mPendingTubeItem = item;
                        mPendingTubeItemPosition = position;
                        AlertDialog.Builder builder = new AlertDialog.Builder(mProgressBar.getContext());
                        builder.setTitle(R.string.msg_video_is_locked);
                        builder.setMessage(R.string.msg_watch_video_ads_to_unlock);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mAd.show();
                                //Show ads
                            }
                        });
                        builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    } else if (!isAdsEnabled) {
                        Toast.makeText(mProgressBar.getContext(), R.string.err_something_went_wrong, Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onActionClick(TubeItem item) {

            }

            @Override
            public void onItemRemoved(TubeItem item, int position) {

            }
        });

        RecyclerView recyclerViewVideoList = (RecyclerView) view.findViewById(R.id.recyclerview_videos);
        mLinearLayoutManager = new LinearLayoutManager(mProgressBar.getContext());
        recyclerViewVideoList.setLayoutManager(mLinearLayoutManager);
        recyclerViewVideoList.setAdapter(mContentListRecyclerAdapter);

        new TubeManager().getVideos(mVideoRequest, new OnOperationListener<List<TubeItem>>() {
            @Override
            public void onOperationListenerSuccess(List<TubeItem> response) {
                if (isAdded()) {
                    updateList(response);
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onOperationListenerEmpty() {
                if (isAdded()) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onOperationListenerError(NetworkErrorThrowable t) {
                if (isAdded()) {
                    Toast.makeText(mProgressBar.getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });

        return view;
    }

    private void loadRewardedVideoAd() {
        if (mAd == null) return;

        String videoAdId;
        if (BuildConfig.DEBUG) {
            videoAdId = FirebaseRemoteConfig.getInstance().getString(Constants.RemoteConfigConstant.AD_MOB_TEST_VIDEO_AD_ID);
        } else {
            videoAdId = FirebaseRemoteConfig.getInstance().getString(Constants.RemoteConfigConstant.AD_MOB_VIDEO_AD_ID);
        }
        mAd.loadAd(videoAdId, new AdRequest.Builder().build());

    }

    private void updateList(List<TubeItem> tubeItems) {
        for (TubeItem tubeItem : tubeItems) {
            if (tubeItem == null || TextUtils.isEmpty(tubeItem.getStringId())) {
                continue;
            }
            mContentListRecyclerAdapter.addItem(tubeItem);
            if (mVideoRequest.currentVideoId.equals(tubeItem.getStringId())) {
                lastPlayingPosition = mContentListRecyclerAdapter.getItemCount() - 1;
                mContentListRecyclerAdapter.updateCurrentStatus(TubeItem.PLAY_STATUS_BUFFERING, lastPlayingPosition);
                mLinearLayoutManager.scrollToPosition(lastPlayingPosition);
            }
        }

        mProgressBar.setVisibility(View.GONE);
    }


    public void updatePlayStatus(int status) {
        mContentListRecyclerAdapter.updateCurrentStatus(status, lastPlayingPosition);

    }

    public void clearPlayBackAnimation() {
        mContentListRecyclerAdapter.clearCurrentPlayingStatus();
    }

    @Override
    public void onPause() {
        if (mAd != null)
            mAd.pause(mProgressBar.getContext());
        saveVideos();
        super.onPause();
    }

    @Override
    public void onResume() {
        if (mAd != null)
            mAd.resume(mProgressBar.getContext());
        super.onResume();

    }

    @Override
    public void onDestroy() {
        if (mAd != null)
            mAd.destroy(mProgressBar.getContext());
        super.onDestroy();
    }


    public void onRewardedVideoAdLoaded() {
        if (isAdded())
            Toast.makeText(mProgressBar.getContext(), R.string.msg_video_ads_available, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }


    @Override
    public void onRewardedVideoAdClosed() {
        if (isAdded()) {
            if (isAdWatched || BuildConfig.DEBUG) {
                isSaveRequired = true;
                isAdWatched = false;
                if (mOnVideoItemClickListener != null) {
                    lastPlayingPosition = mPendingTubeItemPosition;
                    mOnVideoItemClickListener.onItemClick(mPendingTubeItem);
                }
                mContentListRecyclerAdapter.setLocked(TubeItem.LOCK_STATUS_JUST_UNLOCKED, mPendingTubeItemPosition);
            }
            loadRewardedVideoAd();
        }
    }


    @Override
    public void onRewarded(RewardItem rewardItem) {
        if (isAdded()) isSaveRequired = isAdWatched = true;
    }


    @Override
    public void onRewardedVideoAdLeftApplication() {
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
    }


    public void saveVideos() {
        if (isSaveRequired) {
            TubeDb.setVideos(mVideoRequest.categoryId, mContentListRecyclerAdapter.getData());
            isSaveRequired = false;
        }
    }
}


