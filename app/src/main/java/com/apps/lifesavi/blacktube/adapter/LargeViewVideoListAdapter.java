package com.apps.lifesavi.blacktube.adapter;


import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import pl.droidsonroids.gif.GifImageView;

class LargeViewVideoListAdapter extends BaseTubeContentListAdapter {

    @Override
    protected int getViewResourceId(Context context) {
        return R.layout.item_video_large;
    }

    @Override
    protected void onBindView(final ViewHolder holder, TubeItem item, int position) {
        if (item.getSnippet() != null && item.getSnippet().getThumbnails() != null) {
            holder.imageViewThumbnails.setVisibility(View.GONE); //Thumbnail Gone
            holder.gifDrawable.setVisibility(View.VISIBLE);     // Loading drawable on
            String url = item.getSnippet().getThumbnails().getMedium().getUrl(); // parse URL

            holder.textViewViews.setText(item.getStatistics().formattedViewCount);

            Picasso.with(holder.itemView.getContext()).load(url).into(holder.imageViewThumbnails, new Callback() {
                @Override
                public void onSuccess() {
                    holder.gifDrawable.setVisibility(View.GONE);
                    holder.imageViewThumbnails.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {
                    holder.gifDrawable.setVisibility(View.GONE);
                    holder.imageViewThumbnails.setVisibility(View.VISIBLE);
                }
            });


            if (item.lockStatus == TubeItem.LOCK_STATUS_LOCKED) {
                holder.imageViewLocked.setVisibility(View.VISIBLE);
            } else if (item.lockStatus == TubeItem.LOCK_STATUS_JUST_UNLOCKED) {
                holder.imageViewLocked.setVisibility(View.VISIBLE);
                holder.imageViewLocked.animate().translationY(holder.imageViewThumbnails.getHeight()/2).setDuration(1000).alpha(0.0f);
                item.lockStatus = TubeItem.LOCK_STATUS_UNLOCKED;
            } else {
                holder.imageViewLocked.setVisibility(View.GONE);
            }

        }
    }

    @Override
    protected void onViewHolder(ViewHolder viewHolder, View itemView) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mToolBarHeight * 21, mToolBarHeight * 5);

        viewHolder.textViewViews = (TextView) itemView.findViewById(R.id.textview_views_count);
        viewHolder.progressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);
        viewHolder.linearLayoutContentDetails = (LinearLayout) itemView.findViewById(R.id.ll_content_details);
        viewHolder.linearLayoutContentDetails.setVisibility(View.VISIBLE);

        viewHolder.gifDrawable = (GifImageView) itemView.findViewById(R.id.gifview);
        viewHolder.gifDrawable.setLayoutParams(layoutParams);

        viewHolder.imageViewThumbnails.setLayoutParams(layoutParams);
        viewHolder.imageViewThumbnails.setBackgroundResource(R.drawable.background_loading);

    }


    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {

    }
}
