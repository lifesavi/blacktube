package com.apps.lifesavi.blacktube.adapter;


import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.model.Region;
import com.squareup.picasso.Picasso;

;

public class RegionListAdapter extends BaseSimpleListAdapter<Region> {


    @Override
    public void onBindView(BaseSimpleListAdapter.ViewHolder holder, Region region) {
        holder.textViewKey.setText(region.text);
        if (region.isSelected) {
            Drawable icon = ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.ic_circle_region);
            icon.setColorFilter(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            holder.imageViewIcon.setImageDrawable(icon);
            holder.textViewKey.setTextColor(ContextCompat.getColor(holder.imageViewIcon.getContext(),R.color.colorPrimary));

        } else
            Picasso.with(holder.itemView.getContext()).load(R.drawable.ic_circle_empty).fit().into(holder.imageViewIcon);
        holder.textViewKey.setTextColor(ContextCompat.getColor(holder.imageViewIcon.getContext(),R.color.colorWhite));

    }
}
