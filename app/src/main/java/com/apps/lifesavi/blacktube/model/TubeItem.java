package com.apps.lifesavi.blacktube.model;

import com.google.gson.internal.LinkedTreeMap;

public class TubeItem {
    public static final int PLAY_STATUS_STOPPED = 0;
    public static final int PLAY_STATUS_BUFFERING = 0;
    public static final int PLAY_STATUS_PLAYING = 1 + PLAY_STATUS_BUFFERING;
    public static final int PLAY_STATUS_PAUSED = 1 + PLAY_STATUS_PLAYING;

    public static final int LOCK_STATUS_UNLOCKED = 0;
    public static final int LOCK_STATUS_JUST_UNLOCKED = 1 + LOCK_STATUS_UNLOCKED;
    public static final int LOCK_STATUS_LOCKED = 1 + LOCK_STATUS_JUST_UNLOCKED;


    final public static int ITEM_MEDIUM_ICON = 0;
    final public static int ITEM_LARGE_ICON = ITEM_MEDIUM_ICON + 1;

    public Object id;
    public boolean isLocked;
    public int lockStatus;
    private String etag;

    private Snippet snippet;

    private Statistics statistics;

    private BrandSetting brandingSettings;

    private ContentDetails contentDetails;

    private String kind;

    private String playlistId;

    private int playStatus;

    public Object getId() {
        return id;
    }

    public int getPlayStatus() {
        return playStatus;
    }

    public void setPlayStatus(int playStatus) {
        this.playStatus = playStatus;
    }

    public transient boolean isFav = false;
    public boolean showOnHome;

    public ContentDetails getContentDetails() {
        return contentDetails;
    }

    public void setContentDetails(ContentDetails contentDetails) {
        this.contentDetails = contentDetails;
    }

    public void setId(Object id) {
        this.id = id;
    }


    public String getStringId() {
        if (id instanceof String) {
            return id.toString();
        }
        return ((LinkedTreeMap<String, String>) id).get("videoId");
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public void setStatistics(Statistics statistics) {
        this.statistics = statistics;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public BrandSetting getBrandingSettings() {
        return brandingSettings;
    }

    public void setBrandingSettings(BrandSetting brandingSettings) {
        this.brandingSettings = brandingSettings;
    }

    public void setSnippet(Snippet snippet) {
        this.snippet = snippet;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", etag = " + etag + ", snippet = " + snippet + ", kind = " + kind + "]";
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TubeItem tubeItem = (TubeItem) o;

        return getStringId() != null ? getStringId().equals(tubeItem.getStringId()) : tubeItem.getStringId() == null;

    }

    @Override
    public int hashCode() {
        return getStringId() != null ? getStringId().hashCode() : 0;
    }
}