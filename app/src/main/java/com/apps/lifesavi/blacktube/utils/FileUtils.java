package com.apps.lifesavi.blacktube.utils;


import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;

public class FileUtils {
    public static <T> T getFromAssetsFolder(Context context, String filename, Class<T> clazz, Type type) {
        AssetManager manager = context.getAssets();
        BufferedReader reader = null;
        StringBuilder response = new StringBuilder();
        T object = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(manager.open(filename)));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                response.append(mLine);
            }
            if (type != null) {
                object = new Gson().fromJson(response.toString(), type);
            } else {
                object = new Gson().fromJson(response.toString(), clazz);
            }
        } catch (IOException e) {
            //log the exception
            Log.i(IOException.class.getName(), e.getMessage());
        } catch (JsonSyntaxException e) {
            //log the exception
            Log.i(JsonSyntaxException.class.getName(), e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return object;
    }

}
