package com.apps.lifesavi.blacktube.application;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import io.paperdb.Paper;

public class BlackTubeApplication extends Application {

    public static BlackTubeApplication blackTubeApplication;

    public static BlackTubeApplication getInstance() {
        return blackTubeApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Paper.init(this);
        Fabric.with(this, new Crashlytics());
        blackTubeApplication = this;
    }

    public static Context getContext() {
        return blackTubeApplication.getApplicationContext();
    }
}
