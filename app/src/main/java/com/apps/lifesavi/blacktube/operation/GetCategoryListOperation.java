package com.apps.lifesavi.blacktube.operation;

import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.BaseTubeResponse;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.apps.lifesavi.blacktube.constant.Constants.QueryConstants.KEY;
import static com.apps.lifesavi.blacktube.constant.Constants.QueryConstants.QUERY_REGION_CODE;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.YOUTUBE_DEVELOPER_KEY;
import static com.apps.lifesavi.blacktube.constant.Constants.UrlConstants.GET_VIDEO_CATEGORY_LIST;


public class GetCategoryListOperation extends BaseWebOperation<BaseTubeResponse> {

    @Override
    public void onSuccess(BaseTubeResponse response) {
        mOnOperationListener.onOperationListenerSuccess(response);
    }

    @Override
    public void onError(NetworkErrorThrowable t) {
        mOnOperationListener.onOperationListenerError(t);
    }

    public interface OnGetCategoryOperation {
        @GET(GET_VIDEO_CATEGORY_LIST)// Quota cost 3
        Call<BaseTubeResponse> onGetCategoryList(@Query(QUERY_REGION_CODE) String regionCode , @Query(KEY) String key);
    }

    public GetCategoryListOperation(String regionCode, OnOperationListener<BaseTubeResponse> listener) {
        mOnOperationListener = listener;
        getRetrofitInstance().create(OnGetCategoryOperation.class).onGetCategoryList(regionCode, FirebaseRemoteConfig.getInstance().getString(YOUTUBE_DEVELOPER_KEY)).enqueue(this);
    }
}
