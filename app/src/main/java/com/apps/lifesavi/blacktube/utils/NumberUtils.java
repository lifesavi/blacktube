package com.apps.lifesavi.blacktube.utils;


import java.util.Locale;

public class NumberUtils {

    public static String getFormattedCount(long number) {
        double result = (double) number;
        String type = result + "";
        if (result > 999_999_999_999f) {
            result = number / 1_000_000_000_000f;
            type = String.format(Locale.getDefault(), "%.1f", result) + "T";
        } else if (result > 999_999_999f) {
            result = number / 1_000_000_000f;
            type = String.format(Locale.getDefault(), "%.1f", result) + "B";
        } else if (result > 999_999f) {
            result = number / 1_000_000f;
            type = String.format(Locale.getDefault(), "%.1f", result) + "M";
        } else if (result > 999f) {
            result = number / 1_000f;
            type = String.format(Locale.getDefault(), "%.1f", result) + "K";
        } else {
            return number + "";
        }
        return type.replace(".0", "");
    }
}
