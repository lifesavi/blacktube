package com.apps.lifesavi.blacktube.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.fragment.BaseFragment;
import com.apps.lifesavi.blacktube.fragment.DrawerFragment;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import io.fabric.sdk.android.Fabric;

import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.SAVED;

public class HomeActivity extends BaseActivity {

    private DrawerFragment mDrawerFragment;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ic_back_arrow_blue));
        setSupportActionBar(toolbar);
        MobileAds.initialize(HomeActivity.this, FirebaseRemoteConfig.getInstance().getString(Constants.RemoteConfigConstant.AD_MOB_APP_ID, ""));


        //Drawer
        final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(mDrawerToggle);
        displayNavigationIcons(true);
        displayNavigationIcons(false); // Bad coding, Without this Hamburger is not showing :(
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerToggle.getDrawerArrowDrawable().setColor(ContextCompat.getColor(this, R.color.colorWhite));
        mDrawerToggle.syncState();

        mDrawerFragment = DrawerFragment.newInstance();
        mDrawerFragment.setOnItemClickListener(new OnItemClickListener<Integer>() {
            @Override
            public void onItemClick(Integer option) {
                drawerLayout.closeDrawers();
            }
        });
        mDrawerFragment.setOnNavigationToggleListener(new BaseFragment.OnNavigationToggleListener() {
            @Override
            public void onNavigationChange(boolean hasNavigation) {
                toggleNavigation(hasNavigation);
            }
        });

        replace(R.id.framelayout_drawer_container, mDrawerFragment, false);
    }


    @Override
    protected void onResume() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        if (mDrawerFragment != null) mDrawerFragment.updateItem(SAVED);
        super.onResume();
    }

    private void toggleNavigation(boolean hasNavigation) {
        if (hasNavigation) {
            mDrawerToggle.setDrawerIndicatorEnabled(false); // Remove hamburger
            displayNavigationIcons(true);
            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        } else {
            displayNavigationIcons(false);
            mDrawerToggle.setDrawerIndicatorEnabled(true);             // Show hamburger
            mDrawerToggle.setToolbarNavigationClickListener(null);
        }
    }
}
