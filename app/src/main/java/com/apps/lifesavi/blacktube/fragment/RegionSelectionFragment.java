package com.apps.lifesavi.blacktube.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.adapter.RegionListAdapter;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.Region;
import com.apps.lifesavi.blacktube.operation.manager.TubeManager;
import com.apps.lifesavi.blacktube.utils.TubeDb;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.List;


public class RegionSelectionFragment extends BaseFragment {


    private RegionListAdapter adapter;
    private OnItemClickListener<Region> mListener;
    private LinearLayout mLinearLayoutContent;
    private ProgressBar mProgressBar;
    private TextView mTextViewMessage;
    private FirebaseRemoteConfig mFireBaseRemoteConfig;


    public static RegionSelectionFragment newInstance() {
        return new RegionSelectionFragment();
    }

    public void setOnItemClickListener(OnItemClickListener<Region> listener) {
        mListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_region_selection, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        mLinearLayoutContent = (LinearLayout) view.findViewById(R.id.linear_layout_content);

        mTextViewMessage = (TextView) view.findViewById(R.id.textview_limit);

        final List<Region> localRegions = new TubeManager().getLocalRegions();
        adapter = new RegionListAdapter();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_region);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        adapter.setOptionOnItemClickListener(new OnItemClickListener<Region>() {
            @Override
            public void onItemClick(Region region) {
                TubeDb.setSelectedRegionTo(region.id);

                if (!localRegions.contains(region)) {
                    localRegions.add(region);
                    updateRegionText(localRegions.size());
                    TubeDb.setLocalRegions(localRegions);
                }

                if (mListener != null) {
                    mListener.onItemClick(region);
                }

            }
        });

        mFireBaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        if (localRegions.size() >= mFireBaseRemoteConfig.getLong(Constants.RemoteConfigConstant.REGION_SELECTION_LIMIT)) {
            adapter.setData(localRegions);
        } else {
            getRegions();
        }

        updateRegionText(localRegions.size());

        return view;
    }

    private void updateRegionText(int count) {
        if (count >= mFireBaseRemoteConfig.getLong(Constants.RemoteConfigConstant.REGION_SELECTION_LIMIT)) {
            mTextViewMessage.setVisibility(View.VISIBLE);
            mTextViewMessage.setText(getString(R.string.lbl_region_selection_limit_reached));
        } else if (count >= mFireBaseRemoteConfig.getLong(Constants.RemoteConfigConstant.REGION_SELECTION_WARNING_AT)) {
            mTextViewMessage.setVisibility(View.VISIBLE);
            mTextViewMessage.setText(String.format(getString(R.string.lbl_region_warning_message), mFireBaseRemoteConfig.getLong(Constants.RemoteConfigConstant.REGION_SELECTION_LIMIT) - count));  //TODO : To be added
        } else {
            mTextViewMessage.setText(String.format(getString(R.string.lbl_region_warning_message), mFireBaseRemoteConfig.getLong(Constants.RemoteConfigConstant.REGION_SELECTION_LIMIT) - count));  //TODO : To be added
            mTextViewMessage.setVisibility(View.GONE);
        }
    }


    private void getRegions() {
        mLinearLayoutContent.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);
        new TubeManager().getRegions(new OnOperationListener<List<Region>>() {
            @Override
            public void onOperationListenerSuccess(List<Region> response) {
                if (isAdded()) {
                    adapter.setData(response);
                    mLinearLayoutContent.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onOperationListenerEmpty() {
                if (isAdded()) {
                    Toast.makeText(getContext(), "No regions found", Toast.LENGTH_SHORT).show();
                    mLinearLayoutContent.setVisibility(View.VISIBLE);
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onOperationListenerError(NetworkErrorThrowable t) {
                if (isAdded()) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }
}
