package com.apps.lifesavi.blacktube.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.adapter.BaseTubeContentListAdapter;

import tr.xip.errorview.ErrorView;


abstract public class BaseVideoListFragment extends BaseFragment implements BaseTubeContentListAdapter.OnItemClickListener {

    protected ProgressBar mProgressBar;
    protected ErrorView mErrorView;
    protected BaseTubeContentListAdapter mContentListRecyclerAdapter;
    protected RecyclerView mRecyclerViewVideoList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_list, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        mErrorView = (ErrorView) view.findViewById(R.id.error_view);
        mErrorView.setRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                onRetryClick();
            }
        });

        mRecyclerViewVideoList = (RecyclerView) view.findViewById(R.id.recyclerview_videos);
        mContentListRecyclerAdapter = BaseTubeContentListAdapter.getInstance(getType());
        mContentListRecyclerAdapter.setTubeItemListListener(this);
        mRecyclerViewVideoList.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerViewVideoList.setAdapter(mContentListRecyclerAdapter);

        if(isAdded())
        onInit(view);

        return view;
    }

    protected abstract int getType();

    protected abstract void onInit(View view);

    protected abstract void onRetryClick();

}
