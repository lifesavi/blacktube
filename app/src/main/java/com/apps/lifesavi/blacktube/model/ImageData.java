package com.apps.lifesavi.blacktube.model;

public class ImageData {
    private int id ;
    private String keywords;
    public String drawableImage;

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDrawableImage() {
        return drawableImage;
    }

    public void setDrawableImage(String drawableImage) {
        this.drawableImage = drawableImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
