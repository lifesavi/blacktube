package com.apps.lifesavi.blacktube.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.apps.lifesavi.blacktube.activity.HomeActivity;


public class BaseFragment extends DialogFragment {

    public OnNavigationToggleListener mOnNavigationToggleListener;

    public interface OnNavigationToggleListener {
        void onNavigationChange(boolean hasNavigation);
    }

    public void setOnNavigationToggleListener(OnNavigationToggleListener listener) {
        mOnNavigationToggleListener = listener;
    }

    public void replace(int containerId, BaseFragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(containerId, fragment);
        transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    protected void popBackStackUpto(int index) {
        if (getActivity() != null) {
            ((HomeActivity) getActivity()).popAllFragmentsUpto(index);
        }
    }

    protected void setTitle(String title) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (!TextUtils.isEmpty(title) && (actionBar != null)) {
            actionBar.setTitle(title);
        }
    }

    protected void setSubtitle(String subTitle) {
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (!TextUtils.isEmpty(subTitle) && (actionBar != null)) {
            actionBar.setSubtitle(subTitle);
        }
    }

}
