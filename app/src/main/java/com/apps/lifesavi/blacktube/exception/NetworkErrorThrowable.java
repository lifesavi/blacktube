package com.apps.lifesavi.blacktube.exception;


import java.io.IOException;
import java.net.SocketTimeoutException;

public class NetworkErrorThrowable extends Throwable {

    private Throwable mThrowable;
    private String mMessage;

    private static final String ERROR_MSG_NO_INTERNET_CONNECTION = "No Internet Connection.";
    private static final String ERROR_MSG_CONNECTION_TIME_OUT = "Connection time out.";
    private static final String ERROR_MSG_DEFAULT = "Something went wrong.";

    public NetworkErrorThrowable(Throwable throwable) {
        mThrowable = throwable;
        mMessage = extractMessage(throwable);
    }

    public NetworkErrorThrowable(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getStackMessage() {
        if (mThrowable != null)
            return mThrowable.getMessage();
        return "";
    }

    private String extractMessage(Throwable throwable) {
        if (throwable instanceof SocketTimeoutException)
            return ERROR_MSG_CONNECTION_TIME_OUT;
        else if (throwable instanceof IOException)
            return ERROR_MSG_NO_INTERNET_CONNECTION;
        else
            return ERROR_MSG_DEFAULT;
    }
}
