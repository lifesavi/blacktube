package com.apps.lifesavi.blacktube.model;

public class Statistics {
    public long viewCount;
    public long commentCount;
    public boolean hiddenSubscriberCount;
    public long videoCount;
    public long subscriberCount;
    public long likeCount;
    public long dislikeCount;
    public long favoriteCount;
    public String formattedViewCount;
}
