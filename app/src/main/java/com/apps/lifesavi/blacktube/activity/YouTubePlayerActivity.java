package com.apps.lifesavi.blacktube.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.Display;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.apps.lifesavi.blacktube.BuildConfig;
import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.async.SearchSavedVideoAsyncTask;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.fragment.VideoListSuggestionFragment;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.model.VideoRequest;
import com.apps.lifesavi.blacktube.operation.manager.TubeManager;
import com.apps.lifesavi.blacktube.utils.AppUtils;
import com.apps.lifesavi.blacktube.utils.NumberUtils;
import com.apps.lifesavi.blacktube.utils.TubeDb;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.Gson;

import java.util.List;

import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.AD_MOB_INTERSTITIAL_AD_ID;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.AD_MOB_INTERSTITIAL_TEST_AD_ID;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.INTERSTITIAL_AD_AFTER;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.IS_INTERSTITIAL_ENABLED;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.YOUTUBE_DEVELOPER_KEY;
import static com.apps.lifesavi.blacktube.model.TubeItem.ITEM_MEDIUM_ICON;


public class YouTubePlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    //Views
    private YouTubePlayer mYouTubePlayer;
    private YouTubePlayerView mYouTubeView;

    private TextView mTextViewLikeCount;
    private TextView mTextViewDislikeCount;
    private TextView mTextViewViewCount;
    private TextView mTextViewTitle;
    private TextView mTextViewSaveStatus;

    private ProgressBar mProgressBarSaveStatus;
    private ToggleButton mToggleButtonIsSaved;
    private LinearLayout mLinearLayoutSaveStatus;

    //Ads
    private InterstitialAd mInterstitialAd;
    private long mInterstitialAdAfter;
    private long mItemClickCounter;
    private boolean mIsInterstitialEnabled;

    //Data
    private String mVideoId;
    private String mVideoCategoryId;

    private int mSuggestionType;
    private boolean mInitialSaveStatus;
    private boolean isFavSaveRequired;

    private List<TubeItem> mSavedTubeItemList;
    private TubeItem mCurrentVideoItem;
    private VideoListSuggestionFragment mVideoSuggestionFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tube_player);

        mYouTubeView = (YouTubePlayerView) findViewById(R.id.youtube_playerview);
        mYouTubeView.initialize(FirebaseRemoteConfig.getInstance().getString(YOUTUBE_DEVELOPER_KEY), this);

        final LinearLayout linearLayoutShare = (LinearLayout) findViewById(R.id.ll_share);
        linearLayoutShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppUtils.share("", Constants.FORMAT_YOUTUBE_VIDEO + mVideoId, Constants.INTENT_SHARE_BODY_TEXT_TYPE);
            }
        });

        //Get video details
        mVideoId = getIntent().getStringExtra(Constants.INTENT_KEY_VIDEO_ID);
        mVideoCategoryId = getIntent().getStringExtra(Constants.INTENT_KEY_VIDEO_CATEGORY_ID);
        mSuggestionType = getIntent().getIntExtra(Constants.INTENT_SUGGESTION_VIDEO_TYPE, 0);

        if (mSuggestionType == VideoRequest.SUGGESTION_VIDEO_TYPE_SAVED) {
            FloatingActionButton fabShare = (FloatingActionButton) findViewById(R.id.fab_share);
            fabShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    linearLayoutShare.performClick();
                }
            });
            findViewById(R.id.ll_status).setVisibility(View.GONE);
            fabShare.setVisibility(View.VISIBLE);
        }

        mTextViewLikeCount = (TextView) findViewById(R.id.textview_likes_count);
        mTextViewDislikeCount = (TextView) findViewById(R.id.textview_dis_likes_count);
        mTextViewViewCount = (TextView) findViewById(R.id.textview_views_count);
        mTextViewTitle = (TextView) findViewById(R.id.textview_title);
        mTextViewSaveStatus = (TextView) findViewById(R.id.textview_save_status);

        mProgressBarSaveStatus = (ProgressBar) findViewById(R.id.progressbar_save_status);
        mLinearLayoutSaveStatus = (LinearLayout) findViewById(R.id.ll_save);
        mToggleButtonIsSaved = (ToggleButton) findViewById(R.id.toggle_button_is_saved);
        mToggleButtonIsSaved.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mTextViewSaveStatus.setText(isChecked ? R.string.lbl_saved : R.string.lbl_save);
                isFavSaveRequired = true;
            }
        });

        mSavedTubeItemList = new TubeManager().getSavedVideos();
        mCurrentVideoItem = new Gson().fromJson(getIntent().getStringExtra(Constants.INTENT_KEY_CURRENT_ITEM), TubeItem.class);

        FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
        if (mIsInterstitialEnabled = remoteConfig.getBoolean(IS_INTERSTITIAL_ENABLED)) {
            mInterstitialAd = new InterstitialAd(this);
            mInterstitialAd.setAdUnitId(remoteConfig.getString
                    (BuildConfig.DEBUG ? AD_MOB_INTERSTITIAL_TEST_AD_ID : AD_MOB_INTERSTITIAL_AD_ID));
            mInterstitialAdAfter = remoteConfig.getLong(INTERSTITIAL_AD_AFTER);
            mInterstitialAd.loadAd(new AdRequest.Builder().build());
            mInterstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    // Code to be executed when an ad finishes loading.
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // Code to be executed when an ad request fails.
                }

                @Override
                public void onAdOpened() {
                    // Code to be executed when the ad is displayed.
                }

                @Override
                public void onAdLeftApplication() {
                    // Code to be executed when the user has left the app.
                }

                @Override
                public void onAdClosed() {
                    mInterstitialAd.loadAd(new AdRequest.Builder().build());
                    // Code to be executed when when the interstitial ad is closed.
                }
            });
        }

        updateStatusViews();
        checkVideoSaveStatus();
        showSuggestions();
    }

    private void showSuggestions() {
        VideoRequest videoRequest = new VideoRequest();
        videoRequest.categoryId = mVideoCategoryId;
        videoRequest.itemType = ITEM_MEDIUM_ICON;
        videoRequest.currentVideoId = mVideoId;
        videoRequest.suggestionVideoType = mSuggestionType;
        mVideoSuggestionFragment = VideoListSuggestionFragment.newInstance(videoRequest);
        mVideoSuggestionFragment.setOnVideoItemClickListener(new OnItemClickListener<TubeItem>() {
            @Override
            public void onItemClick(TubeItem tubeItem) {
                if (mYouTubePlayer != null) {
                    updatePreviousVideo();
                    //Assign Values
                    mCurrentVideoItem = tubeItem;
                    mVideoId = tubeItem.getStringId();
                    //Update Video Status
                    updateStatusViews();
                    checkVideoSaveStatus();
                    //Play Video
                    mYouTubePlayer.loadVideo(mVideoId);

                    mItemClickCounter++;
                    checkForInterstitialAd();
                }
            }
        });


        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.framelayout_video_list_container, mVideoSuggestionFragment, "");
        transaction.commit();
    }

    private void checkForInterstitialAd() {
        if (mIsInterstitialEnabled && mInterstitialAd.isLoaded() && mItemClickCounter >= mInterstitialAdAfter) {
            mItemClickCounter = 0;
            mInterstitialAd.show();
        }
    }

    private void updatePreviousVideo() {
        if (mInitialSaveStatus != mToggleButtonIsSaved.isChecked()) {
            if (mInitialSaveStatus) {
                mSavedTubeItemList.remove(mCurrentVideoItem);
            } else {
                mSavedTubeItemList.add(0, mCurrentVideoItem);
            }
            mInitialSaveStatus = mToggleButtonIsSaved.isChecked();
            isFavSaveRequired = true;
        }
    }

    private void updateSavedVideoList() {
        if (mSavedTubeItemList != null) {
            TubeDb.setSaveVideos(mSavedTubeItemList);
            TubeDb.setSaveCount(mSavedTubeItemList.size());
        }
    }


    @Override
    protected void onUserLeaveHint() {
        saveFavVideos();
        super.onUserLeaveHint();
    }

    private void saveFavVideos() {
        if (isFavSaveRequired) {
            updatePreviousVideo();
            updateSavedVideoList();
            isFavSaveRequired = false;
        }
    }

    @Override
    protected void onPause() {
        saveFavVideos();
        super.onPause();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT && mVideoSuggestionFragment != null) {
            mVideoSuggestionFragment.mContentListRecyclerAdapter.update();
        }
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        if (getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
            if(mYouTubePlayer!=null){
                mYouTubePlayer.setFullscreen(false);
            }
        } else {
            saveFavVideos();
            super.onBackPressed();
        }
    }

    private void checkVideoSaveStatus() {
        mProgressBarSaveStatus.setVisibility(View.VISIBLE);
        mLinearLayoutSaveStatus.setVisibility(View.GONE);
        SearchSavedVideoAsyncTask asyncTask = new SearchSavedVideoAsyncTask(mSavedTubeItemList);
        asyncTask.setOnSearchCompleteListener(new SearchSavedVideoAsyncTask.OnSearchCompleteListener() {
            @Override
            public void onSearchComplete(boolean isPresent) {
                mInitialSaveStatus = isPresent;
                mProgressBarSaveStatus.setVisibility(View.GONE);
                mLinearLayoutSaveStatus.setVisibility(View.VISIBLE);
                mToggleButtonIsSaved.setChecked(isPresent);
            }
        });
        asyncTask.execute(mVideoId);
    }


    private void updateStatusViews() {
        if (mCurrentVideoItem.getStatistics().likeCount > 0) {
            mTextViewLikeCount.setVisibility(View.VISIBLE);
            mTextViewLikeCount.setText(NumberUtils.getFormattedCount(mCurrentVideoItem.getStatistics().likeCount));
        } else {
            mTextViewLikeCount.setVisibility(View.GONE);
        }
        if (mCurrentVideoItem.getStatistics().dislikeCount > 0) {
            mTextViewDislikeCount.setVisibility(View.VISIBLE);
            mTextViewDislikeCount.setText(NumberUtils.getFormattedCount(mCurrentVideoItem.getStatistics().dislikeCount));
        } else {
            mTextViewDislikeCount.setVisibility(View.GONE);
        }

        mTextViewViewCount.setText(NumberUtils.getFormattedCount(mCurrentVideoItem.getStatistics().viewCount));
        mTextViewTitle.setText(mCurrentVideoItem.getSnippet().getTitle());
    }

    protected int getScreenOrientation() {
        Display getOrient = getWindowManager().getDefaultDisplay();
        Point size = new Point();

        getOrient.getSize(size);

        int orientation;
        if (size.x < size.y) {
            orientation = Configuration.ORIENTATION_PORTRAIT;
        } else {
            orientation = Configuration.ORIENTATION_LANDSCAPE;
        }
        return orientation;
    }


    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, 0).show();
        } else {
            Toast.makeText(this, errorReason.toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        final YouTubePlayer player, boolean wasRestored) {
        if (!wasRestored) {
            player.loadVideo(mVideoId);
        }
        player.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
            @Override
            public void onFullscreen(boolean b) {
                if (!b) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
            }
        });

        player.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
            @Override
            public void onLoading() {

            }

            @Override
            public void onLoaded(String s) {

            }

            @Override
            public void onAdStarted() {

            }

            @Override
            public void onVideoStarted() {

            }

            @Override
            public void onVideoEnded() {
                if (mVideoSuggestionFragment != null) {
                    mVideoSuggestionFragment.clearPlayBackAnimation();
                }
            }

            @Override
            public void onError(YouTubePlayer.ErrorReason errorReason) {
                if (mVideoSuggestionFragment != null) {
                    mVideoSuggestionFragment.clearPlayBackAnimation();
                }
            }
        });
        player.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
            @Override
            public void onPlaying() {
                if (mVideoSuggestionFragment != null) {
                    mVideoSuggestionFragment.updatePlayStatus(TubeItem.PLAY_STATUS_PLAYING);
                }
            }

            @Override
            public void onPaused() {
                if (mVideoSuggestionFragment != null) {
                    mVideoSuggestionFragment.updatePlayStatus(TubeItem.PLAY_STATUS_PAUSED);
                }
            }

            @Override
            public void onStopped() {
                if (mVideoSuggestionFragment != null) {
                    mVideoSuggestionFragment.clearPlayBackAnimation();
                }
            }

            @Override
            public void onBuffering(boolean b) {
                if (mVideoSuggestionFragment != null)
                    mVideoSuggestionFragment.updatePlayStatus(TubeItem.PLAY_STATUS_BUFFERING);
            }


            @Override
            public void onSeekTo(int i) {
                if (i == player.getDurationMillis()) {
                    onStopped();
                }
            }
        });
        mYouTubePlayer = player;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mYouTubeView.initialize(FirebaseRemoteConfig.getInstance().getString(YOUTUBE_DEVELOPER_KEY), this);
    }

}
