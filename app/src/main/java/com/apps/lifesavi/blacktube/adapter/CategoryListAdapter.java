package com.apps.lifesavi.blacktube.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.squareup.picasso.Picasso;


public class CategoryListAdapter extends BaseTubeContentListAdapter {

    @Override
    protected int getViewResourceId(Context context) {
        return R.layout.item_catagory;
    }

    @Override
    protected void onBindView(ViewHolder holder, TubeItem item, int position) {
        if (item.getSnippet().getThumbnails() != null) {
            final int resourceId = mContext.getResources().getIdentifier(item.getSnippet().getThumbnails().getLocalDrawablePath(), Constants.SYSTEM_RESOURCE_DRAWABLE, mContext.getPackageName());
            if (resourceId > 0)
                Picasso.with(holder.itemView.getContext()).load(resourceId).fit().into(holder.imageViewThumbnails);
        }

    }

    @Override
    protected void onViewHolder(ViewHolder viewHolder, View itemView) {
        viewHolder.progressBar = (ProgressBar)itemView.findViewById(R.id.progressbar);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

    }

    @Override
    public void onItemDismiss(int position) {

    }
}
