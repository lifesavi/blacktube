package com.apps.lifesavi.blacktube.model;

import static com.apps.lifesavi.blacktube.model.TubeItem.ITEM_LARGE_ICON;

public class VideoRequest {

    public VideoRequest() {
        this.categoryId = "0";
        this.title = "Most Popular";
        this.itemType = ITEM_LARGE_ICON;
        this.suggestionVideoType = VideoRequest.SUGGESTION_VIDEO_TYPE_CATEGORY;
    }

    public static int SUGGESTION_VIDEO_TYPE_CATEGORY = 0;
    public static int SUGGESTION_VIDEO_TYPE_SAVED = SUGGESTION_VIDEO_TYPE_CATEGORY + 1;

    public String categoryId;
    public String title;
    public int itemType;
    public int suggestionVideoType;
    public boolean hasNavigation;
    public String currentVideoId;

}
