package com.apps.lifesavi.blacktube.interfaces;


public interface OnItemClickListener<T> {
    void onItemClick(T t);
}
