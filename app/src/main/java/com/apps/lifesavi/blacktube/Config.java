package com.apps.lifesavi.blacktube;

public class Config {
    public static final int BUILD_TYPE_DEVELOPMENT = 0;
    public static final int BUILD_TYPE_PRODUCTION = BUILD_TYPE_DEVELOPMENT + 1;

    //Ads
    public static final boolean IS_ADS_ENABLED = false;
    public static final int REGION_SELECTION_LIMIT = 5;
    public static final int REGION_SELECTION_WARNING_AT = 3;
    public static final int MAX_SAVE_LIMIT = 200;

    public static int BUILD_TYPE = BUILD_TYPE_DEVELOPMENT;
}
