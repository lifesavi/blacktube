package com.apps.lifesavi.blacktube.model;


public class Option {
    public final static int OPTION_SAVED_VIDEO = 1;
    public final static int OPTION_REGION_SELECTION = OPTION_SAVED_VIDEO + 1;
    public final static int OPTION_CLEAR_PREFERENCE = OPTION_REGION_SELECTION + 1;
    public final static int OPTION_RATE_APP = 99;
    public final static int OPTION_FEEDBACK = OPTION_RATE_APP + 1;

    public int id;
    public String text;
    public String subText;
    public String drawableImage;
    public boolean isSelected;

    public Option() {

    }

    public Option(int id, String subText) {
        this.id = id;
        this.subText = subText;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Option option = (Option) o;

        return id == option.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
