package com.apps.lifesavi.blacktube.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;


abstract public class BaseSimpleListAdapter<T> extends RecyclerView.Adapter<BaseSimpleListAdapter.ViewHolder> {

    private List<T> mOptionList = new ArrayList<>();
    private OnItemClickListener<T> mOptionOnItemClickListener;

    public void setOptionOnItemClickListener(OnItemClickListener<T> listener) {
        mOptionOnItemClickListener = listener;
    }

    @Override
    public BaseSimpleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BaseSimpleListAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_setting, parent, false));
    }

    @Override
    public void onBindViewHolder(BaseSimpleListAdapter.ViewHolder holder, int position) {
        onBindView(holder, getItem(position));
    }

    abstract public void onBindView(BaseSimpleListAdapter.ViewHolder viewHolder, T t);

    public void setData(List<T> optionList) {
        mOptionList.clear();
        mOptionList.addAll(optionList);
        notifyDataSetChanged();
    }

    private T getItem(int position) {
        return mOptionList.get(position);
    }

    @Override
    public int getItemCount() {
        return mOptionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewKey;
        TextView textViewValue;
        ImageView imageViewIcon;

         ViewHolder(View itemView) {
            super(itemView);
            textViewKey = (TextView) itemView.findViewById(R.id.textview_key);
            textViewValue = (TextView) itemView.findViewById(R.id.textview_value);
            imageViewIcon = (ImageView) itemView.findViewById(R.id.imageview_option_icon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOptionOnItemClickListener != null) {
                        mOptionOnItemClickListener.onItemClick(getItem(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
