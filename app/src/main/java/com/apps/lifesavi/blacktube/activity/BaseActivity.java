package com.apps.lifesavi.blacktube.activity;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.apps.lifesavi.blacktube.fragment.BaseFragment;

public class BaseActivity extends AppCompatActivity {


    public void popAllFragmentsUpto(int index) {
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        for (int i = 0; i < count - index; ++i) {
            fm.popBackStack();
        }
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        } else {
            finish();
        }
    }

    public void displayNavigationIcons(boolean displayNavigation) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(displayNavigation);
    }

    public void replace(int containerId, BaseFragment fragment) {
        replace(containerId, fragment, true);
    }

    public void add(int containerId, BaseFragment fragment) {
        add(containerId, fragment, true);
    }

    /**
     * Method for adding fragment
     *
     * @param containerId Container of the fragment
     * @param fragment    The fragment to be added
     */
    public void add(int containerId, BaseFragment fragment, boolean isFirstTime) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(containerId, fragment, Integer.toString(getSupportFragmentManager().getBackStackEntryCount()));
        if (isFirstTime) ft.addToBackStack(null);
        ft.commit();
    }

    public void replace(int containerId, BaseFragment fragment, boolean addToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment, Integer.toString(getSupportFragmentManager().getBackStackEntryCount()));
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        ft.commit();
        fm.executePendingTransactions();
    }

}
