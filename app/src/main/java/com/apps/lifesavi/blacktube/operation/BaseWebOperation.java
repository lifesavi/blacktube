package com.apps.lifesavi.blacktube.operation;

import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.apps.lifesavi.blacktube.constant.Constants.UrlConstants.BASE_URL;


public abstract class BaseWebOperation<T> implements Callback<T> {

    protected OnOperationListener<T> mOnOperationListener;

    public Retrofit getRetrofitInstance(String baseUrl) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(5, TimeUnit.SECONDS);
        builder.connectTimeout(3, TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        builder.addInterceptor(interceptor);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(interceptor);

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }

    public Retrofit getRetrofitInstance() {
        return getRetrofitInstance(BASE_URL);
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (mOnOperationListener != null)
            onSuccess(response.body());
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (mOnOperationListener != null) {
            onError(new NetworkErrorThrowable(t));
        }
    }

    abstract public void onSuccess(T t);

    abstract public void onError(NetworkErrorThrowable t);
}


