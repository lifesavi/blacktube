package com.apps.lifesavi.blacktube.operation.manager;

import com.apps.lifesavi.blacktube.async.GetCategoryAsyncTask;
import com.apps.lifesavi.blacktube.async.GetVideoBasedOnCategoryAsync;
import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.BaseTubeResponse;
import com.apps.lifesavi.blacktube.model.Region;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.model.VideoRequest;
import com.apps.lifesavi.blacktube.operation.GetRegionListOperation;
import com.apps.lifesavi.blacktube.utils.TubeDb;

import java.util.ArrayList;
import java.util.List;

import static com.apps.lifesavi.blacktube.model.VideoRequest.SUGGESTION_VIDEO_TYPE_CATEGORY;
import static com.apps.lifesavi.blacktube.utils.TubeDb.getSelectedRegionCode;
import static com.apps.lifesavi.blacktube.utils.TubeDb.setRegions;

public class TubeManager {

    public void getCategories(final OnOperationListener<List<TubeItem>> listener) {
        new GetCategoryAsyncTask().setOnOperationListener(new OnOperationListener<List<TubeItem>>() {
            @Override
            public void onOperationListenerSuccess(List<TubeItem> response) {
                if (listener != null) {
                    listener.onOperationListenerSuccess(response);
                }

            }

            @Override
            public void onOperationListenerEmpty() {
                if (listener != null) {
                    listener.onOperationListenerEmpty();
                }
            }

            @Override
            public void onOperationListenerError(NetworkErrorThrowable t) {
                if (listener != null) {
                    listener.onOperationListenerError(t);
                }
            }
        }).execute();
    }


    public List<TubeItem> getSavedVideos() {
        List<TubeItem> responseList = new ArrayList<>();
        for (TubeItem tubeItem : TubeDb.getSaveVideos()) {
            if (tubeItem != null && tubeItem.getStringId() != null) {
                responseList.add(tubeItem);
            }
        }
        return responseList;
    }

    public void getVideos(VideoRequest videoRequest, final OnOperationListener<List<TubeItem>> listener) {
        if (videoRequest.suggestionVideoType == SUGGESTION_VIDEO_TYPE_CATEGORY) {
            GetVideoBasedOnCategoryAsync async = new GetVideoBasedOnCategoryAsync();
            async.setOnOperationListener(new OnOperationListener<List<TubeItem>>() {
                @Override
                public void onOperationListenerSuccess(List<TubeItem> response) {
                    if (listener != null) {
                        listener.onOperationListenerSuccess(response);
                    }
                }

                @Override
                public void onOperationListenerEmpty() {
                    if (listener != null) {
                        listener.onOperationListenerEmpty();
                    }
                }

                @Override
                public void onOperationListenerError(NetworkErrorThrowable t) {
                    if (listener != null) {
                        listener.onOperationListenerError(t);
                    }
                }
            });
            async.execute(videoRequest.categoryId);
        } else {
            if (listener != null) {
                listener.onOperationListenerSuccess(getSavedVideos());
            }
        }
    }

    public List<Region> getLocalRegions() {
        List<Region> regionList = TubeDb.getLocalRegions();
        final String selectedRegion = getSelectedRegionCode();
        for (Region region : regionList) {
            region.isSelected = false;
            if (selectedRegion.equals(region.id)) {
                region.isSelected = true;
            }
        }
        return regionList;
    }

    public void getRegions(final OnOperationListener<List<Region>> listener) {

        final List<Region> regionList = TubeDb.getRegions();
        final String selectedRegion = getSelectedRegionCode();

        if (regionList != null && regionList.size() > 0) {
            for (Region region : regionList) {
                region.isSelected = region.id.equals(selectedRegion);
            }
            listener.onOperationListenerSuccess(regionList);
        } else {
            new GetRegionListOperation(new OnOperationListener<BaseTubeResponse>() {
                @Override
                public void onOperationListenerSuccess(final BaseTubeResponse baseTubeResponse) {
                    if (listener != null) {
                        final String selectedRegion = getSelectedRegionCode();
                        for (TubeItem tubeItem : baseTubeResponse.getItems()) {


                            if (tubeItem.getStringId().equals("IN")) {
                                tubeItem.getSnippet().setRegionName("India");
                            }

                            Region region = new Region();
                            region.id = tubeItem.getStringId();
                            region.text = tubeItem.getSnippet().getRegionName();
                            if (tubeItem.getStringId().equals(selectedRegion)) {
                                region.isSelected = true;
                            }
                            regionList.add(region);
                        }
                        setRegions(regionList);
                        listener.onOperationListenerSuccess(regionList);
                    }
                }

                @Override
                public void onOperationListenerEmpty() {
                    if (listener != null) {
                        setRegions(new ArrayList<Region>());
                        listener.onOperationListenerEmpty();
                    }
                }

                @Override
                public void onOperationListenerError(NetworkErrorThrowable t) {
                    if (listener != null) {
                        listener.onOperationListenerError(t);
                    }
                }
            });
        }
    }
}

