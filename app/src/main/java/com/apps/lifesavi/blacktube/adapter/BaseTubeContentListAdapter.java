package com.apps.lifesavi.blacktube.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.apps.lifesavi.blacktube.Config;
import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.utils.AppUtils;
import com.apps.lifesavi.blacktube.utils.SimpleItemTouchHelperCallback;

import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

import static com.apps.lifesavi.blacktube.model.TubeItem.ITEM_MEDIUM_ICON;

abstract public class BaseTubeContentListAdapter extends RecyclerView.Adapter<BaseTubeContentListAdapter.ViewHolder> implements SimpleItemTouchHelperCallback.ItemTouchHelperAdapter {


    protected Context mContext;
    private int mCurrentPlayingPosition = -1;
    private OnItemClickListener mOnItemClickListener;
    private List<TubeItem> mTubeList = new ArrayList<>();
    protected int mToolBarHeight = 0;


    private TubeItem mLastRemovedItem;
    private int mLastRemovedItemPosition;

    protected abstract int getViewResourceId(Context context);

    protected abstract void onBindView(ViewHolder holder, TubeItem item, int position);

    public static BaseTubeContentListAdapter getInstance(int itemType) {
        return itemType == ITEM_MEDIUM_ICON ? new MediumViewVideoListAdapter() : new LargeViewVideoListAdapter();
    }

    public void undoLastDelete() {
        mTubeList.add(mLastRemovedItemPosition, mLastRemovedItem);
        notifyItemInserted(mLastRemovedItemPosition);
    }

    public interface OnItemClickListener {
        void onItemClick(TubeItem item, int position);

        void onActionClick(TubeItem item);

        void onItemRemoved(TubeItem item, int position);
    }

    public void addItem(TubeItem tubeItem) {
        mTubeList.add(tubeItem);
        notifyItemInserted(mTubeList.size() - 1);
    }

    public List<TubeItem> getData() {
        return mTubeList;
    }

    public void setTubeItemListListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }


    public int getCurrentPlayingPosition() {
        return mCurrentPlayingPosition;
    }

    public boolean setLocked(int status, int position) {
        if (position > -1) {
            mTubeList.get(position).lockStatus = status;
            notifyItemChanged(position);
            return true;
        }
        return false;
    }

    public void updateCurrentStatus(int status, int currentPlayingPosition) {
        if (currentPlayingPosition > -1 && currentPlayingPosition < mTubeList.size()) {
            this.mCurrentPlayingPosition = currentPlayingPosition;
            mTubeList.get(currentPlayingPosition).setPlayStatus(status);
            notifyItemChanged(currentPlayingPosition);
        }
    }

    public void clearCurrentPlayingStatus() {
        if (mCurrentPlayingPosition > -1) {
            int lastPos = mCurrentPlayingPosition;
            mTubeList.get(lastPos).setPlayStatus(TubeItem.PLAY_STATUS_STOPPED);
            mCurrentPlayingPosition = -1;
            notifyItemChanged(lastPos);
        }
    }

    public void update() {
        if (mCurrentPlayingPosition > -1) {
            notifyItemChanged(mCurrentPlayingPosition);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        mToolBarHeight = AppUtils.getToolBarHeight();
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(getViewResourceId(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TubeItem item = getItem(position);
        holder.textViewTitle.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorWhite));
        if (item != null && item.getSnippet() != null) {
            holder.textViewTitle.setText(item.getSnippet().getTitle());

        if (item.getContentDetails() != null && holder.textViewDuration != null)
            holder.textViewDuration.setText(item.getContentDetails().getFormattedDuration());

            onBindView(holder, item, position);
        }
    }

    public void setData(List<TubeItem> list) {
        mTubeList.clear();
        mTubeList.addAll(list);
        notifyDataSetChanged();
    }

    public void clear() {
        mTubeList.clear();
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mLastRemovedItem = mTubeList.get(position);
        mLastRemovedItemPosition = position;
        mTubeList.remove(position);
        notifyItemRemoved(position);
        mOnItemClickListener.onItemRemoved(mLastRemovedItem, position);
    }

    private TubeItem getItem(int position) {
        return mTubeList.get(position);
    }

    @Override
    public int getItemCount() {
        return mTubeList.size() > Config.MAX_SAVE_LIMIT ? Config.MAX_SAVE_LIMIT : mTubeList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewDetails;
        TextView textViewViews;
        TextView textViewDuration;

        ImageView imageViewLocked;
        ImageView imageViewThumbnails;

        ProgressBar progressBar;
        ToggleButton buttonAction;

        GifImageView gifDrawable;
        LinearLayout linearLayoutContentDetails;

        ViewHolder(View itemView) {
            super(itemView);
            imageViewThumbnails = (ImageView) itemView.findViewById(R.id.imageview_thumbnail);
            textViewTitle = (TextView) itemView.findViewById(R.id.textview_title);
            textViewDuration = (TextView) itemView.findViewById(R.id.textview_duration);
            imageViewLocked = (ImageView) itemView.findViewById(R.id.imageview_locked);
            onViewHolder(this, itemView);
            if (buttonAction != null) {
                buttonAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mOnItemClickListener != null && getAdapterPosition() > -1) {
                            mOnItemClickListener.onActionClick(getItem(getAdapterPosition()));
                        }
                    }
                });


                buttonAction.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        getItem(getAdapterPosition()).showOnHome = isChecked;
                    }
                });
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null && getAdapterPosition() > -1) {
                        mOnItemClickListener.onItemClick(getItem(getAdapterPosition()), getAdapterPosition());
                    }
                }
            });
        }
    }

    protected abstract void onViewHolder(ViewHolder viewHolder, View itemView);
}
