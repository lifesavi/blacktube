package com.apps.lifesavi.blacktube.constant;


public final class Constants {

    public final static int MAX_RESULTS = 50;

    public final static String APP_NAME = "BlackTube";
    public final static String INTENT_KEY_VIDEO_ID = "videoId";
    public final static String INTENT_KEY_VIDEO_CATEGORY_ID = "categoryId";
    public final static String SYSTEM_RESOURCE_DRAWABLE = "drawable";
    public final static String INTENT_SUGGESTION_VIDEO_TYPE = "INTENT_SUGGESTION_VIDEO_TYPE";
    public final static String INTENT_SHARE_BODY_TEXT_TYPE = "text/plain";
    public final static String INTENT_KEY_CURRENT_ITEM = "INTENT_KEY_CURRENT_ITEM";
    public final static String FORMAT_YOUTUBE_VIDEO = "https://m.youtube.com/watch?v=";

    public static class RemoteConfigConstant {
        public static final String IS_VIDEO_ADS_ENABLED = "IS_VIDEO_ADS_ENABLED";
        public static final String IS_PRO_LINK_ENABLED = "IS_PRO_LINK_ENABLED";
        public static final String TOTAL_ADS_PER_CATEGORY = "TOTAL_ADS_PER_CATEGORY";
        public static final String REGION_SELECTION_LIMIT = "REGION_SELECTION_LIMIT";
        public static final String REGION_SELECTION_WARNING_AT = "REGION_SELECTION_WARNING_AT";
        public static final String IS_INTERSTITIAL_ENABLED = "IS_INTERSTITIAL_ENABLED";
        public static final String INTERSTITIAL_AD_AFTER = "INTERSTITIAL_AD_AFTER";

        public final static String YOUTUBE_DEVELOPER_KEY = "YOUTUBE_DEVELOPER_KEY";
        public final static String PRO_APP_LINK = "PRO_APP_LINK";

        public final static String AD_MOB_APP_ID = "AD_MOB_APP_ID";
        public final static String AD_MOB_VIDEO_AD_ID = "AD_MOB_VIDEO_AD_ID";
        public final static String AD_MOB_TEST_VIDEO_AD_ID = "AD_MOB_TEST_VIDEO_AD_ID";
        public final static String AD_MOB_INTERSTITIAL_AD_ID = "AD_MOB_INTERSTITIAL_AD_ID";
        public final static String AD_MOB_INTERSTITIAL_TEST_AD_ID = "AD_MOB_INTERSTITIAL_TEST_AD_ID";

        public static final int DEFAULT_ADS_COUNT = 2;
        public static final String CURRENT_APP_VERSION_CODE = "CURRENT_APP_VERSION_CODE";
        public static final String SHOW_APP_UPDATE_POP_UP = "SHOW_APP_UPDATE_POP_UP";


    }

    public static class UrlConstants {
        public final static String BASE_URL = "https://www.googleapis.com/youtube/v3/";
        public final static String GET_VIDEO_CATEGORY_LIST = "videoCategories?part=snippet";
        public final static String GET_VIDEO_LIST_BASED_ON_CATEGORY = "videos?part=snippet%2CcontentDetails%2Cstatistics&chart=mostPopular&maxResults=" + Constants.MAX_RESULTS ;
        public final static String GET_REGION_LIST = "i18nRegions?part=snippet&hl=es_MX&fields=items(id%2Csnippet%2Fname)";
    }

    public static class QueryConstants {
        public final static String KEY = "key";
        public final static String QUERY_REGION_CODE = "regionCode";
        public final static String QUERY_VIDEO_CATEGORY_ID = "videoCategoryId";

    }

    public static class PreferenceConstants {
        public final static String QUOTA_COST_COUNT = "QUOTA_COST_COUNT";
        public final static String PREF_KEY_CURRENT_DATE = "PREF_KEY_CURRENT_DATE";
    }

    public static class VideoRequestBundleConstant {
        public final static String CATEGORY_ID = "CATEGORY_ID";
        public final static String TITLE = "TITLE";
        public final static String ITEM_TYPE = "ITEM_TYPE";
        public final static String SUGGESTION_VIDEO_TYPE = "SUGGESTION_VIDEO_TYPE";
        public final static String HAS_NAVIGATION = "HAS_NAVIGATION";
        public final static String CURRENT_VIDEO_ID = "CURRENT_VIDEO_ID";

    }

    public static class PaperDbConstants {
        //DB NAMES
        public final static String PAPER_DB_CONSISTENT = "PAPER_DB_CONSISTENT";

        //DB LABELS
        public final static String PAPER_DB_LABEL_CATEGORIES = "PAPER_DB_LABEL_CATEGORIES";
        public final static String PAPER_DB_LABEL_DEFAULT_REGION_CODE = "IN";
        public final static String PAPER_DB_LABEL_CURRENT_CHANNEL_ITEM = "PAPER_DB_LABEL_CURRENT_CHANNEL_ITEM";

        //DB KEYS
        public final static String PAPER_DB_KEY_REGIONS = "PAPER_DB_KEY_REGIONS";
        public final static String PAPER_DB_KEY_REGION_CODE = "PAPER_DB_KEY_REGION_CODE";
        public final static String PAPER_DB_KEY_SAVED_VIDEO = "PAPER_DB_KEY_SAVED_VIDEO";
        public final static String PAPER_DB_KEY_SAVED_COUNT = "PAPER_DB_KEY_SAVED_COUNT";
        public final static String PAPER_DB_KEY_RECENT_REGIONS = "PAPER_DB_KEY_RECENT_REGIONS";
    }

    public static class DrawerConstants {
        public final static int HOME = 0;
        public final static int SAVED = 1;
        public final static int REGION = 2;
        public final static int PREMIUM = 6;
        public final static int SHARE = 7;
        public final static int RATE_APPLICATION = 8;
        public final static int FEEDBACK = 9;
        public final static int CLEAR_PREFERENCE = 91;
    }

}
