package com.apps.lifesavi.blacktube.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;

import com.apps.lifesavi.blacktube.application.BlackTubeApplication;
import com.apps.lifesavi.blacktube.constant.Constants;

import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.REGION;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.SAVED;
import static com.apps.lifesavi.blacktube.utils.TubeDb.getSaveCount;
import static com.apps.lifesavi.blacktube.utils.TubeDb.getSelectedRegionCode;

public class AppUtils {

    public static void share(String title, String body, String type) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType(type);
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, title);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, body + "\nShared via : BlackTube - Get latest trending videos :\nhttps://play.google.com/store/apps/details?id=" + BlackTubeApplication.getContext().getPackageName());
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent intent = Intent.createChooser(sharingIntent, "Share using");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        BlackTubeApplication.getInstance().startActivity(intent);
    }

    public static void shareAppLink() {
        final String appPackageName = BlackTubeApplication.getContext().getPackageName();
        Intent intentSharing = new Intent(Intent.ACTION_SEND);
        intentSharing.setType("text/plain");
        String body = "BlackTube - Get latest trending videos :\nhttps://play.google.com/store/apps/details?id=" + appPackageName;
        intentSharing.putExtra(Intent.EXTRA_TEXT, body);
        intentSharing.putExtra(Intent.EXTRA_SUBJECT, Constants.APP_NAME);
        intentSharing.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent intent = Intent.createChooser(intentSharing, "Share via");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        BlackTubeApplication.getInstance().startActivity(intent);
    }

    public static void sendFeedback() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "lifesavi@gmail.com", null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Suggestion for improvement");
        emailIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent intent = Intent.createChooser(emailIntent, "Send Suggestion Using Email");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        BlackTubeApplication.getInstance().startActivity(intent);
    }

    public static void openAppInPlayStore() {
        final String appPackageName = BlackTubeApplication.getInstance().getPackageName(); // getPackageName() from Context or Activity object
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
            sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            BlackTubeApplication.getInstance().startActivity(sharingIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Intent sharingIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
            sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            BlackTubeApplication.getInstance().startActivity(sharingIntent);
        }
    }

    public static void openProApp(String appPackageName) {
        if (TextUtils.isEmpty(appPackageName)) {
            return;
        }
        try {
            Intent sharingIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
            sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            BlackTubeApplication.getInstance().startActivity(sharingIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Intent sharingIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
            sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            BlackTubeApplication.getInstance().startActivity(sharingIntent);
        }
    }

    public static String getCategoryRegionKey(String categoryId, String regionId) {
        return regionId + categoryId;
    }

    public static String getDrawerSubText(int id) {
        String subText = "";
        if (id == SAVED) {
            int savedVideoCount = getSaveCount();
            if (savedVideoCount > 0) {
                subText = savedVideoCount + " Videos";
            }
        } else if (id == REGION) {
            subText = getSelectedRegionCode();
        }
        return subText;
    }

    public static int getToolBarHeight() {
        final Resources resources = BlackTubeApplication.getContext().getResources();
        final int resourceId = resources.getIdentifier("action_bar_size", "dimen", "android");
        return resourceId > 0 ?
                resources.getDimensionPixelSize(resourceId) :
                (int) convertDpToPixel(BlackTubeApplication.getContext(), 48);
    }

    public static float convertDpToPixel(Context context, float dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return dp * scale + 0.5f;
    }

}
