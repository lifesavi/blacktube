package com.apps.lifesavi.blacktube.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.apps.lifesavi.blacktube.BuildConfig;
import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.utils.AppUtils;
import com.apps.lifesavi.blacktube.utils.DateTimeUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import io.paperdb.Paper;

import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.CURRENT_APP_VERSION_CODE;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.SHOW_APP_UPDATE_POP_UP;

public class SplashScreenActivity extends AppCompatActivity {

    private FirebaseRemoteConfig mFireBaseRemoteConfig;
    private CoordinatorLayout mCoordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatelayout);
        FirebaseApp.initializeApp(this);

        mFireBaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG).build();
        mFireBaseRemoteConfig.setConfigSettings(configSettings);
        mFireBaseRemoteConfig.setDefaults(R.xml.remote_config_default_values);

        //Initialise Paper
        Paper.init(this);
        if (!Paper.book().read(DateTimeUtils.getCurrentDate() + "", false)) {
            Paper.book().destroy();
            Paper.init(this);
            getConfigData();
        } else {
            proceedToHome();
        }

    }

    private void getConfigData() {
        long cacheExpiration = 3600;
        if (mFireBaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        mFireBaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Paper.book().write(DateTimeUtils.getCurrentDate() + "", true);
                    mFireBaseRemoteConfig.activateFetched();
                    if (mFireBaseRemoteConfig.getBoolean(SHOW_APP_UPDATE_POP_UP) &&
                            mFireBaseRemoteConfig.getLong(CURRENT_APP_VERSION_CODE) > BuildConfig.VERSION_CODE) {
                        showUpdateDialog();
                    } else {
                        proceedToHome();
                    }

                } else {
                    showSnackBar(getString(R.string.err_check_net_connection));
                }
            }
        });
    }

    private void showUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreenActivity.this);
        builder.setTitle(R.string.msg_new_update_available);
        builder.setCancelable(false);
        builder.setMessage(R.string.msg_update_detail);
        builder.setPositiveButton(R.string.lbl_update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                proceedToHome();
                AppUtils.openAppInPlayStore();
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.lbl_later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                proceedToHome();
            }
        });
        builder.show();
    }

    private void proceedToHome() {
        startActivity(new Intent(SplashScreenActivity.this, HomeActivity.class));
        finish();
    }

    private void showSnackBar(String message) {
        final Snackbar snackbar = Snackbar.make(mCoordinatorLayout, message + "", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
                getConfigData();
            }
        });
        snackbar.show();
    }

}
