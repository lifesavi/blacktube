package com.apps.lifesavi.blacktube.model;

public class Id
{
    private String id;

    private String kind;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getKind ()
    {
        return kind;
    }

    public void setKind (String kind)
    {
        this.kind = kind;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+ id +", kind = "+kind+"]";
    }
}