package com.apps.lifesavi.blacktube.model;

import com.google.gson.annotations.SerializedName;

public class Thumbnails {
    @SerializedName("default")
    private Image defaultImage;

    private Image high;

    private Image medium;

    private String localDrawablePath;

    public Image getDefault() {
        return defaultImage;
    }

    public void setDefault(Image defaultImage) {
        this.defaultImage = defaultImage;
    }

    public Image getDefaultImage() {
        return defaultImage;
    }

    public String getLocalDrawablePath() {
        return localDrawablePath;
    }

    public void setLocalDrawablePath(String localDrawablePath) {
        this.localDrawablePath = localDrawablePath;
    }

    public void setDefaultImage(Image defaultImage) {
        this.defaultImage = defaultImage;
    }

    public Image getHigh() {
        return high;
    }

    public void setHigh(Image high) {
        this.high = high;
    }

    public Image getMedium() {
        return medium;
    }

    public void setMedium(Image medium) {
        this.medium = medium;
    }

    @Override
    public String toString() {
        return "ClassPojo [defaultImage = " + defaultImage + ", high = " + high + ", medium = " + medium + "]";
    }
}

			