package com.apps.lifesavi.blacktube.adapter;


import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.squareup.picasso.Picasso;

import java.util.Collections;

import pl.droidsonroids.gif.GifImageView;

public class MediumViewVideoListAdapter extends BaseTubeContentListAdapter {


    @Override
    protected int getViewResourceId(Context context) {
        return R.layout.item_video_medium;
    }

    @Override
    protected void onBindView(ViewHolder holder, TubeItem item, int position) {
        String url = item.getSnippet().getThumbnails().getMedium().getUrl();
        Picasso.with(holder.itemView.getContext()).load(url).resize(mToolBarHeight * 3, mToolBarHeight * 2).centerCrop().into(holder.imageViewThumbnails);
        if (position == getCurrentPlayingPosition()) {
            holder.gifDrawable.setVisibility(View.VISIBLE);
            if (item.getPlayStatus() == TubeItem.PLAY_STATUS_BUFFERING) {
                holder.gifDrawable.setBackgroundResource(R.drawable.loading_anim);
                holder.textViewDetails.setText(R.string.lbl_buffering);
            } else if (item.getPlayStatus() == TubeItem.PLAY_STATUS_PAUSED) {
                holder.gifDrawable.setBackgroundResource(R.drawable.black_pause);
                holder.textViewDetails.setText(R.string.lbl_paused);
            } else {
                holder.gifDrawable.setBackgroundResource(R.drawable.equi_anim);
                holder.textViewDetails.setText(R.string.lbl_playing);
            }
        } else {
            holder.gifDrawable.setVisibility(View.GONE);
            holder.textViewDetails.setText(item.getSnippet().getChannelTitle() + "\n" + String.format("%s Views", item.getStatistics().formattedViewCount));
        }

        if (item.lockStatus == TubeItem.LOCK_STATUS_LOCKED) {
            holder.imageViewLocked.setVisibility(View.VISIBLE);
        } else if (item.lockStatus == TubeItem.LOCK_STATUS_JUST_UNLOCKED) {
            holder.imageViewLocked.setVisibility(View.VISIBLE);
            holder.imageViewLocked.animate().translationY(holder.imageViewThumbnails.getHeight() / 2).setDuration(1000).alpha(0.0f);
            item.lockStatus = TubeItem.LOCK_STATUS_UNLOCKED;
        } else {
            holder.imageViewLocked.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onViewHolder(ViewHolder viewHolder, View itemView) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mToolBarHeight * 3, mToolBarHeight * 2);

        viewHolder.progressBar = (ProgressBar) itemView.findViewById(R.id.progressbar);
        viewHolder.textViewDetails = (TextView) itemView.findViewById(R.id.textview_description);

        viewHolder.gifDrawable = (GifImageView) itemView.findViewById(R.id.gifview);
        viewHolder.gifDrawable.setLayoutParams(layoutParams);

        viewHolder.imageViewThumbnails.setLayoutParams(layoutParams);
        viewHolder.imageViewThumbnails.setBackgroundResource(R.drawable.background_loading);
    }

    private void swap(int firstPosition, int secondPosition) {
        Collections.swap(getData(), firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        swap(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        super.remove(position);
    }

}
