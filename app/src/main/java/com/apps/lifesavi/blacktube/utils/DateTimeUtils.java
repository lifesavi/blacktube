package com.apps.lifesavi.blacktube.utils;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeUtils {
    public final static String DATE_FORMAT_DD_MM_YYYY_HH_MM_A = "dd-MM-yyyy | HH:mm";
    public final static String DATE_FORMAT_YYYY_MM_DD_T_HH_MM_SS = "yyyy-MM-dd'T'HH:mm:ss";
    public final static String DATE_FORMAT_DD_MM_YYYY = "dd-MM-yyyy";
    public final static String DATE_FORMAT_HH_MM_SS = "HH:mm:ss";


    public static long getDuration(String time) {
        time = time.substring(2);
        long duration = 0L;
        Object[][] indexs = new Object[][]{{"H", 3600}, {"M", 60}, {"S", 1}};
        for (int i = 0; i < indexs.length; i++) {
            int index = time.indexOf((String) indexs[i][0]);
            if (index != -1) {
                String value = time.substring(0, index);
                try {
                    duration += Integer.parseInt(value) * (int) indexs[i][1] * 1000;
                } catch (NumberFormatException ignored){ }
                time = time.substring(value.length() + 1);
            }
        }
        return duration;
    }

    public static String getYouTubeFormattedTime(String duration) {
        String time = DateTimeUtils.getFormattedStringTime(DateTimeUtils.getDuration(duration), DateTimeUtils.DATE_FORMAT_HH_MM_SS);
        if (time.length() > 5 && time.substring(0, 2).equals("00")) {
            time = time.replaceFirst("00:", "");
        }
        return time;
    }

    public static long getTimeStampFromDate(String date, String format) {
        long timeStamp = 0;
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        try {
            timeStamp = 0; //sdf.parse(date).getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeStamp;
    }

    public static String getFormattedStringTime(long timeStamp, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(timeStamp);
    }

    public static String convertFromFormat(String date, String fromFormat, String toFormat) {
        return getFormattedStringTime(getTimeStampFromDate(date, fromFormat), toFormat);
    }


    public static long getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }


}
