package com.apps.lifesavi.blacktube.model;


public class Region {
    public String id;
    public String text;
    public boolean isSelected;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Region region = (Region) o;

        return id != null ? id.equals(region.id) : region.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
