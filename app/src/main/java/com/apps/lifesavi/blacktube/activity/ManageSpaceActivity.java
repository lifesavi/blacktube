package com.apps.lifesavi.blacktube.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.constant.Constants;

import io.paperdb.Paper;

public class ManageSpaceActivity extends AppCompatActivity implements View.OnClickListener {

    private Switch switchSavedVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_space_activty);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Drawable icon = ContextCompat.getDrawable(this, R.drawable.ic_back_arrow_blue);
        toolbar.setNavigationIcon(icon);

        final TextView textViewSavedVideo = (TextView) findViewById(R.id.textview_saved_video);
        switchSavedVideo = (Switch) findViewById(R.id.switch_saved_video);
        switchSavedVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                textViewSavedVideo.setTextColor(ContextCompat.getColor(getApplicationContext(), isChecked ? R.color.colorYouRed : R.color.colorWhite));
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_cancel) {
            finish();
        } else if (v.getId() == R.id.button_clear) {
            Paper.init(getApplicationContext());
            if (switchSavedVideo.isChecked()) {
                Paper.book(Constants.PaperDbConstants.PAPER_DB_CONSISTENT).destroy();
                Toast.makeText(this, "Videos Cleared", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}
