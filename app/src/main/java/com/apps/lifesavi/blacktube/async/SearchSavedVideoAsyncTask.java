package com.apps.lifesavi.blacktube.async;

import android.os.AsyncTask;

import com.apps.lifesavi.blacktube.model.TubeItem;

import java.util.List;



public class SearchSavedVideoAsyncTask extends AsyncTask<String, Void, Boolean> {

    private OnSearchCompleteListener mOnSearchCompleteListener;
    private List<TubeItem> mTubeItemList;

    public SearchSavedVideoAsyncTask(List<TubeItem> tubeItems) {
        mTubeItemList = tubeItems;
    }

    public interface OnSearchCompleteListener {
        void onSearchComplete(boolean isPresent);
    }

    public void setOnSearchCompleteListener(OnSearchCompleteListener listener) {
        mOnSearchCompleteListener = listener;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String videoId = params[0];
        boolean hasItem = false;
        for (TubeItem item : mTubeItemList) {
            if (item != null && item.getStringId() != null && videoId.equals(item.getStringId())) {
                hasItem = true;
                break;
            }
        }
        return hasItem;
    }

    @Override
    protected void onPostExecute(Boolean isPresent) {
        if (mOnSearchCompleteListener != null) {
            mOnSearchCompleteListener.onSearchComplete(isPresent);
        }
    }


}
