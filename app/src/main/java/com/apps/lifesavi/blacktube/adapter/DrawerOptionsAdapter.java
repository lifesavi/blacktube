package com.apps.lifesavi.blacktube.adapter;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;
import com.apps.lifesavi.blacktube.model.Option;

import java.util.ArrayList;
import java.util.List;

public class DrawerOptionsAdapter extends RecyclerView.Adapter<DrawerOptionsAdapter.ViewHolder> {

    private List<Option> mOptionList = new ArrayList<>();
    private OnItemClickListener<Option> mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener<Option> listener) {
        mOnItemClickListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_text, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Option option = getItem(position);

        holder.textViewOption.setText(option.text);

        if (!TextUtils.isEmpty(option.subText)) {
            holder.textViewDescription.setVisibility(View.VISIBLE);
            holder.textViewDescription.setText(option.subText);
        } else {
            holder.textViewDescription.setVisibility(View.GONE);
        }

        int resourceId = holder.itemView.getResources().getIdentifier(option.drawableImage, Constants.SYSTEM_RESOURCE_DRAWABLE, holder.itemView.getContext().getPackageName());
        if (resourceId > -1) {
            Drawable icon = ContextCompat.getDrawable(holder.itemView.getContext(),resourceId);
            icon.setColorFilter(ContextCompat.getColor(holder.itemView.getContext(),R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            holder.imageViewOptionIcon.setImageDrawable(icon);
        }
    }

    public void setData(List<Option> list) {
        mOptionList.clear();
        mOptionList.addAll(list);
        notifyDataSetChanged();
    }

    public void updateItem(Option option) {
        int index = mOptionList.indexOf(option);
        Option dummy = getItem(index);
        dummy.subText = option.subText;
        mOptionList.set(index, dummy);
        notifyItemChanged(index);
    }

    private Option getItem(int index) {
        if (index > -1 && index < getItemCount()) {
            return mOptionList.get(index);
        }
        return new Option(-1, "BlackTube");
    }

    public List<Option> getData() {
        return mOptionList;
    }

    @Override
    public int getItemCount() {
        return mOptionList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewOption;
        TextView textViewDescription;
        ImageView imageViewOptionIcon;

        ViewHolder(View itemView) {
            super(itemView);
            textViewOption = (TextView) itemView.findViewById(R.id.textview_option);
            imageViewOptionIcon = (ImageView) itemView.findViewById(R.id.imageview_option_icon);
            textViewDescription = (TextView) itemView.findViewById(R.id.textview_description);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(getItem(getAdapterPosition()));
                    }
                }
            });
        }
    }

}
