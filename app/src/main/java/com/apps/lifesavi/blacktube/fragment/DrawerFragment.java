package com.apps.lifesavi.blacktube.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.apps.lifesavi.blacktube.BuildConfig;
import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.adapter.DrawerOptionsAdapter;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;
import com.apps.lifesavi.blacktube.model.Option;
import com.apps.lifesavi.blacktube.model.Region;
import com.apps.lifesavi.blacktube.utils.AppUtils;
import com.apps.lifesavi.blacktube.utils.FileUtils;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.CLEAR_PREFERENCE;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.FEEDBACK;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.HOME;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.PREMIUM;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.RATE_APPLICATION;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.REGION;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.SAVED;
import static com.apps.lifesavi.blacktube.constant.Constants.DrawerConstants.SHARE;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.PRO_APP_LINK;

public class DrawerFragment extends BaseFragment {

    private DrawerOptionsAdapter mOptionsAdapter;

    private int mCurrentFragmentType = -1;
    private OnItemClickListener<Integer> mOnItemClickListener;

    public static DrawerFragment newInstance() {
        return new DrawerFragment();
    }

    public void setOnItemClickListener(OnItemClickListener<Integer> mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_drawer, container, false);

        mOptionsAdapter = new DrawerOptionsAdapter();
        RecyclerView recyclerViewDrawer = (RecyclerView) view.findViewById(R.id.recyclerview_drawer);
        recyclerViewDrawer.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewDrawer.setAdapter(mOptionsAdapter);
        mOptionsAdapter.setData(getOptionList());
        mOptionsAdapter.setOnItemClickListener(new OnItemClickListener<Option>() {
            @Override
            public void onItemClick(Option option) {
                setFragment(option.id);
            }
        });

        setFragment(HOME);
        return view;
    }

    private List<Option> getOptionList() {
        List<Option> optionList = FileUtils.getFromAssetsFolder(getContext(), "optionlist_v2.json", null, new TypeToken<List<Option>>() {
        }.getType());

        List<Option> finalOptionList = new ArrayList<>();

        boolean isPrimeOptionAvailable = FirebaseRemoteConfig.getInstance().getBoolean(Constants.RemoteConfigConstant.IS_PRO_LINK_ENABLED);

        for (Option option : optionList) {

            if (!BuildConfig.DEBUG && option.id == Constants.DrawerConstants.CLEAR_PREFERENCE) {
                continue;
            }

            if (!isPrimeOptionAvailable && option.id == Constants.DrawerConstants.PREMIUM) {
                continue;
            }

            if(option.id==REGION){
                option.subText = AppUtils.getDrawerSubText(REGION);
            }


            finalOptionList.add(option);


        }
        return finalOptionList;
    }

    private void setFragment(int type) {
        if (type != REGION && type == mCurrentFragmentType) return;

        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(type);
        }

        switch (type) {
            case HOME:
                popBackStackUpto(0);
                mCurrentFragmentType = type;
                CategoryListFragment fragment = CategoryListFragment.newInstance();
                fragment.setOnNavigationToggleListener(mOnNavigationToggleListener);
                replace(R.id.framelayout_main_container, fragment);
                break;
            case SAVED:
                popBackStackUpto(0);
                mCurrentFragmentType = type;
                SavedVideoListFragment videoListFragment = SavedVideoListFragment.newInstance();
                videoListFragment.setOnNavigationToggleListener(mOnNavigationToggleListener);
                videoListFragment.setOnCountChangeListener(new SavedVideoListFragment.OnCountChangeListener() {
                    @Override
                    public void onCountChange(int count) {
                        updateItem(SAVED);
                    }
                });
                replace(R.id.framelayout_main_container, videoListFragment);
                break;
            case CLEAR_PREFERENCE:
                Paper.book().destroy();
                Paper.init(getContext());
                Toast.makeText(getContext(), "Pref cleared", Toast.LENGTH_SHORT).show();
                break;
            case REGION:
                final RegionSelectionFragment regionSelectionFragment = RegionSelectionFragment.newInstance();
                regionSelectionFragment.setOnItemClickListener(new OnItemClickListener<Region>() {
                    @Override
                    public void onItemClick(Region notToBeUsed) {
                        Toast.makeText(getContext(), notToBeUsed.text, Toast.LENGTH_SHORT).show();
                        updateItem(REGION);
                        regionSelectionFragment.dismiss();
                    }
                });
                regionSelectionFragment.show(getActivity().getSupportFragmentManager(), "show");
                break;

            case PREMIUM:
                AppUtils.openProApp(FirebaseRemoteConfig.getInstance().getString(PRO_APP_LINK));
                break;

            case SHARE:
                AppUtils.shareAppLink();
                break;
            case RATE_APPLICATION:
                AppUtils.openAppInPlayStore();
                break;
            case FEEDBACK:
                AppUtils.sendFeedback();
                break;

        }
    }

    public void updateItem(int type) {
        mOptionsAdapter.updateItem(new Option(type, AppUtils.getDrawerSubText(type)));
    }


}


