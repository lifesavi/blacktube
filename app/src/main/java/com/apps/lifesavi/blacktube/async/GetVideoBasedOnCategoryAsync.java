package com.apps.lifesavi.blacktube.async;

import android.os.AsyncTask;

import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.BaseTubeResponse;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.operation.GetCategoryVideoOperation;
import com.apps.lifesavi.blacktube.utils.DateTimeUtils;
import com.apps.lifesavi.blacktube.utils.NumberUtils;
import com.apps.lifesavi.blacktube.utils.TubeDb;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class GetVideoBasedOnCategoryAsync extends AsyncTask<String, Void, List<TubeItem>> {

    private String mCategoryId;
    private OnOperationListener<List<TubeItem>> monOperationListener;

    public void setOnOperationListener(OnOperationListener<List<TubeItem>> listener) {
        this.monOperationListener = listener;
    }

    @Override
    protected List<TubeItem> doInBackground(String... params) {
        return TubeDb.getVideos(mCategoryId = params[0]);
    }

    @Override
    protected void onPostExecute(List<TubeItem> tubeItems) {

        final boolean isAdEnabled = FirebaseRemoteConfig.getInstance()
                .getBoolean(Constants.RemoteConfigConstant.IS_VIDEO_ADS_ENABLED);

        long totalAd = FirebaseRemoteConfig.getInstance()
                .getLong(Constants.RemoteConfigConstant.TOTAL_ADS_PER_CATEGORY);

        if (totalAd > Constants.MAX_RESULTS) {
            totalAd = Constants.RemoteConfigConstant.DEFAULT_ADS_COUNT;
        }

        long adThreshold = 0;

        if(totalAd > 1){
           adThreshold = Constants.MAX_RESULTS / totalAd;
        }

        if (tubeItems != null) {
            if (!isAdEnabled) {
                for (TubeItem tubeItem : tubeItems) {
                    tubeItem.lockStatus = TubeItem.LOCK_STATUS_UNLOCKED;
                }
            }

            if (tubeItems.size() > 0)
                monOperationListener.onOperationListenerSuccess(tubeItems);
            else
                monOperationListener.onOperationListenerEmpty();

        } else {
            final long finalAdThreshold = adThreshold;
            new GetCategoryVideoOperation(TubeDb.getSelectedRegionCode(), mCategoryId, new OnOperationListener<BaseTubeResponse>() {
                @Override
                public void onOperationListenerSuccess(BaseTubeResponse response) {
                    if (monOperationListener == null)
                        return;
                    if (response.getItems() != null) {

                        if (response.getItems().size() > 0) {
                            List<TubeItem> tubeItems = new ArrayList<>(response.getItems());
                            Collections.sort(tubeItems, new Comparator<TubeItem>() {
                                @Override
                                public int compare(TubeItem o1, TubeItem o2) {
                                    long viewCount1 = 0, viewCount2 = 0;
                                    if (o2.getStatistics() != null) {
                                        viewCount2 = o2.getStatistics().viewCount;
                                    }
                                    if (o1.getStatistics() != null) {
                                        viewCount1 = o1.getStatistics().viewCount;
                                    }
                                    return Long.compare(viewCount2, viewCount1);
                                }
                            });

                            int i = 0;
                            List<TubeItem> itemList = new ArrayList<>();


                            for (TubeItem tubeItem : tubeItems) {
                                if (finalAdThreshold > 0 && i % finalAdThreshold == 0 && isAdEnabled) {
                                    tubeItem.lockStatus = TubeItem.LOCK_STATUS_LOCKED;
                                }

                                if (tubeItem != null && tubeItem.getContentDetails() != null) {
                                    tubeItem.getContentDetails().setFormattedDuration(DateTimeUtils.getYouTubeFormattedTime(tubeItem.getContentDetails().getDuration()));
                                    tubeItem.getStatistics().formattedViewCount = NumberUtils.getFormattedCount(tubeItem.getStatistics().viewCount);
                                    itemList.add(tubeItem);
                                    i++;
                                }
                            }
                            TubeDb.setVideos(mCategoryId, itemList);
                            monOperationListener.onOperationListenerSuccess(itemList);
                        } else {
                            TubeDb.setVideos(mCategoryId, new ArrayList<TubeItem>());
                            monOperationListener.onOperationListenerEmpty();
                        }
                    } else {
                        monOperationListener.onOperationListenerError(new NetworkErrorThrowable("No Videos Found"));
                    }
                }

                @Override
                public void onOperationListenerEmpty() {
                    TubeDb.setVideos(mCategoryId, new ArrayList<TubeItem>());
                    if (monOperationListener != null) {
                        monOperationListener.onOperationListenerEmpty();
                    }
                }

                @Override
                public void onOperationListenerError(NetworkErrorThrowable t) {
                    if (monOperationListener != null)
                        monOperationListener.onOperationListenerError(t);
                }
            });
        }
    }
}
