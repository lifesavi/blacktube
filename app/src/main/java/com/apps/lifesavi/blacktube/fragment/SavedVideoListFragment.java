package com.apps.lifesavi.blacktube.fragment;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.activity.YouTubePlayerActivity;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.interfaces.OnItemClickListener;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.model.VideoRequest;
import com.apps.lifesavi.blacktube.operation.manager.TubeManager;
import com.apps.lifesavi.blacktube.utils.SimpleItemTouchHelperCallback;
import com.apps.lifesavi.blacktube.utils.TubeDb;
import com.google.gson.Gson;

import java.util.List;
import io.paperdb.Paper;

import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_LABEL_CURRENT_CHANNEL_ITEM;
import static com.apps.lifesavi.blacktube.model.TubeItem.ITEM_MEDIUM_ICON;
import static com.apps.lifesavi.blacktube.utils.TubeDb.setSaveCount;
import static com.apps.lifesavi.blacktube.utils.TubeDb.setSaveVideos;


public class SavedVideoListFragment extends BaseVideoListFragment implements OnItemClickListener<TubeItem> {

    private CoordinatorLayout mCoordinatorLayout;

    public static SavedVideoListFragment newInstance() {
        return new SavedVideoListFragment();
    }

    protected OnCountChangeListener mOnCountChangeListener;

    public interface OnCountChangeListener {
        void onCountChange(int count);
    }

    public void setOnCountChangeListener(OnCountChangeListener listener) {
        mOnCountChangeListener = listener;
    }

    @Override
    protected int getType() {
        return ITEM_MEDIUM_ICON;
    }

    @Override
    protected void onInit(View view) {
        setSubtitle(getString(R.string.lbl_saved_videos));

        view.findViewById(R.id.ll_suggestion).setVisibility(View.VISIBLE);

        if (mOnNavigationToggleListener != null) {
            mOnNavigationToggleListener.onNavigationChange(false);
        }

        mCoordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatelayout);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mContentListRecyclerAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(mRecyclerViewVideoList);

        mContentListRecyclerAdapter.setTubeItemListListener(this);
        mContentListRecyclerAdapter.setData(getSavedVideo());

    }

    @Override
    protected void onRetryClick() {

    }

    private void showItemRemovedSnackBar(String channel) {
        Snackbar snackbar = Snackbar.make(mCoordinatorLayout, channel + " removed from", Snackbar.LENGTH_LONG);
        snackbar.setAction("UNDO", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSaveCount(mContentListRecyclerAdapter.getItemCount() + 1);
                mContentListRecyclerAdapter.undoLastDelete();
                mOnCountChangeListener.onCountChange(mContentListRecyclerAdapter.getItemCount());
            }
        });
        snackbar.show();
    }


    private List<TubeItem> getSavedVideo() {
        return new TubeManager().getSavedVideos();
    }

    @Override
    public void onItemClick(TubeItem item, int position) {
        Intent intent = new Intent(getActivity(), YouTubePlayerActivity.class);
        intent.putExtra(Constants.INTENT_KEY_VIDEO_ID, item.getStringId());
        intent.putExtra(Constants.INTENT_SUGGESTION_VIDEO_TYPE, VideoRequest.SUGGESTION_VIDEO_TYPE_SAVED);
        intent.putExtra(Constants.INTENT_KEY_CURRENT_ITEM,new Gson().toJson(item));
        startActivity(intent);
    }

    @Override
    public void onActionClick(TubeItem item) {

    }

    @Override
    public void onItemRemoved(TubeItem item, int position) {
        setSaveCount(mContentListRecyclerAdapter.getData().size());
        mOnCountChangeListener.onCountChange(mContentListRecyclerAdapter.getData().size());
        showItemRemovedSnackBar(item.getSnippet().getTitle() + "");
    }

    @Override
    public void onItemClick(TubeItem tubeItem) {

    }

    @Override
    public void onPause() {
        setSaveVideos(mContentListRecyclerAdapter.getData());
        setSaveCount(mContentListRecyclerAdapter.getData().size());
        super.onPause();
    }
}
