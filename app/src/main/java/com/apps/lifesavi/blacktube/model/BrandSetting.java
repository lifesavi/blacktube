package com.apps.lifesavi.blacktube.model;

import com.google.api.services.youtube.model.ImageSettings;

public class BrandSetting {
    public ImageSettings image;

    public ImageSettings getImage() {
        return image;
    }

    public void setImage(ImageSettings image) {
        this.image = image;
    }
}
