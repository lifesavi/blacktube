package com.apps.lifesavi.blacktube.interfaces;


import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;

public interface OnOperationListener<T> {
    void onOperationListenerSuccess(T response);
    void onOperationListenerEmpty();
    void onOperationListenerError(NetworkErrorThrowable t);
}
