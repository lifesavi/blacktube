package com.apps.lifesavi.blacktube.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.apps.lifesavi.blacktube.R;
import com.apps.lifesavi.blacktube.adapter.BaseTubeContentListAdapter;
import com.apps.lifesavi.blacktube.adapter.CategoryListAdapter;
import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.model.VideoRequest;
import com.apps.lifesavi.blacktube.operation.manager.TubeManager;

import java.util.ArrayList;
import java.util.List;

import tr.xip.errorview.ErrorView;

public class CategoryListFragment extends BaseFragment {

    private ErrorView mErrorView;
    private ProgressBar mProgressBar;
    private CategoryListAdapter mCategoryListAdapter;

    public static CategoryListFragment newInstance() {
        return new CategoryListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category_list, container, false);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        mErrorView = (ErrorView) view.findViewById(R.id.error_view);
        mErrorView.setRetryListener(new ErrorView.RetryListener() {
            @Override
            public void onRetry() {
                getCategoriesForRegion();
            }
        });

        setSubtitle(getString(R.string.lbl_trending));

        if (mOnNavigationToggleListener != null) {
            mOnNavigationToggleListener.onNavigationChange(false);
        }

        mCategoryListAdapter = new CategoryListAdapter();
        mCategoryListAdapter.setTubeItemListListener(new BaseTubeContentListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TubeItem item, int pos) {
                VideoRequest videoRequest = getVideoRequestInstance(item);

                VideoListFragment fragment = VideoListFragment.newInstance(videoRequest);
                fragment.setOnNavigationToggleListener(mOnNavigationToggleListener);
                replace(R.id.framelayout_main_container, fragment);
            }

            @Override
            public void onActionClick(TubeItem item) {

            }

            @Override
            public void onItemRemoved(TubeItem item, int position) {

            }
        });

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_category);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(mCategoryListAdapter);

        getCategoriesForRegion();

        return view;
    }

    private VideoRequest getVideoRequestInstance(TubeItem item) {
        VideoRequest videoRequest = new VideoRequest();
        videoRequest.categoryId = item.getStringId();
        videoRequest.title = item.getSnippet().getTitle();
        videoRequest.hasNavigation = true;
        return videoRequest;
    }

    private void getCategoriesForRegion() {
        mProgressBar.setVisibility(View.VISIBLE);
        mErrorView.setVisibility(View.GONE);
        new TubeManager().getCategories(new OnOperationListener<List<TubeItem>>() {
            @Override
            public void onOperationListenerSuccess(List<TubeItem> response) {
                if (isAdded()) {
                    mCategoryListAdapter.setData(response);
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onOperationListenerEmpty() {
                if (isAdded()) {
                    mCategoryListAdapter.setData(new ArrayList<TubeItem>());
                    mProgressBar.setVisibility(View.GONE);
                    mErrorView.setVisibility(View.VISIBLE);
                    mErrorView.setSubtitle("No Categories founds");
                }
            }

            @Override
            public void onOperationListenerError(NetworkErrorThrowable t) {
                if (isAdded()) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    mErrorView.setVisibility(View.VISIBLE);
                    mErrorView.setSubtitle(t.getMessage());
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }

}
