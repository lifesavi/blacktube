package com.apps.lifesavi.blacktube.model;


public class ContentDetails {
    private PlayList relatedPlaylists;
    private String duration;
    private String formattedDuration;

    public String getDuration() {
        return duration;
    }

    public String getFormattedDuration() {
        return formattedDuration;
    }

    public void setFormattedDuration(String formattedDuration) {
        this.formattedDuration = formattedDuration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public PlayList getRelatedPlaylists() {
        return relatedPlaylists;
    }

    public void setRelatedPlaylists(PlayList relatedPlaylists) {
        this.relatedPlaylists = relatedPlaylists;
    }
}
