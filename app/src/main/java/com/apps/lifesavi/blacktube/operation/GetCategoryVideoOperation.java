package com.apps.lifesavi.blacktube.operation;

import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.BaseTubeResponse;
import com.apps.lifesavi.blacktube.utils.TubePreference;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.apps.lifesavi.blacktube.constant.Constants.QueryConstants.KEY;
import static com.apps.lifesavi.blacktube.constant.Constants.QueryConstants.QUERY_REGION_CODE;
import static com.apps.lifesavi.blacktube.constant.Constants.QueryConstants.QUERY_VIDEO_CATEGORY_ID;
import static com.apps.lifesavi.blacktube.constant.Constants.RemoteConfigConstant.YOUTUBE_DEVELOPER_KEY;
import static com.apps.lifesavi.blacktube.constant.Constants.UrlConstants.GET_VIDEO_LIST_BASED_ON_CATEGORY;

public class GetCategoryVideoOperation extends BaseWebOperation<BaseTubeResponse> {
    @Override
    public void onSuccess(BaseTubeResponse response) {
        mOnOperationListener.onOperationListenerSuccess(response);
    }

    @Override
    public void onError(NetworkErrorThrowable t) {
        mOnOperationListener.onOperationListenerError(t);
    }


    public interface OnGetCategoryVideoOperation {
        @GET(GET_VIDEO_LIST_BASED_ON_CATEGORY)
        Call<BaseTubeResponse> onGetCategoryVideoList(@Query(QUERY_REGION_CODE) String regionCode, @Query(QUERY_VIDEO_CATEGORY_ID) String videoId, @Query(KEY) String key);
    }

    public GetCategoryVideoOperation(String regionCode, String videoCategoryId, OnOperationListener<BaseTubeResponse> listener) {
        mOnOperationListener = listener;
        getRetrofitInstance().create(OnGetCategoryVideoOperation.class).onGetCategoryVideoList(regionCode, videoCategoryId, FirebaseRemoteConfig.getInstance().getString(YOUTUBE_DEVELOPER_KEY)).enqueue(this);
        TubePreference.getInstance().incrementQuotaCountBy(3);
    }
}
