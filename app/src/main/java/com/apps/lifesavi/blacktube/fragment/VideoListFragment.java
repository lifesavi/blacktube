package com.apps.lifesavi.blacktube.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.apps.lifesavi.blacktube.BuildConfig;
import com.apps.lifesavi.blacktube.activity.YouTubePlayerActivity;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.model.VideoRequest;
import com.apps.lifesavi.blacktube.operation.manager.TubeManager;
import com.apps.lifesavi.blacktube.utils.TubeDb;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.gson.Gson;

import java.util.List;

import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.CATEGORY_ID;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.CURRENT_VIDEO_ID;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.HAS_NAVIGATION;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.ITEM_TYPE;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.SUGGESTION_VIDEO_TYPE;
import static com.apps.lifesavi.blacktube.constant.Constants.VideoRequestBundleConstant.TITLE;
import static com.apps.lifesavi.blacktube.model.TubeItem.ITEM_LARGE_ICON;


public class VideoListFragment extends BaseVideoListFragment implements RewardedVideoAdListener {

    private RewardedVideoAd mAd;
    private boolean isAdWatched;
    private boolean isFreshCall;
    private boolean isSaveReq;
    private int recentClickPosition;
    protected VideoRequest mVideoRequest;

    public static VideoListFragment newInstance(VideoRequest videoRequest) {
        VideoListFragment videoListFragment = new VideoListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CATEGORY_ID, videoRequest.categoryId);
        bundle.putString(TITLE, videoRequest.title);
        bundle.putInt(SUGGESTION_VIDEO_TYPE, videoRequest.suggestionVideoType);
        bundle.putInt(ITEM_TYPE, videoRequest.itemType);
        bundle.putBoolean(HAS_NAVIGATION, videoRequest.hasNavigation);
        videoListFragment.setArguments(bundle);
        return videoListFragment;
    }

    @Override
    public void onPause() {
        mAd.pause(getContext());
        super.onPause();
    }


    @Override
    public void onResume() {
        if (isFreshCall) {
            getVideos();
            isFreshCall = false;
        }
        mAd.resume(getContext());
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        saveVideos();
    }

    private void saveVideos() {
        if (isSaveReq) {
            TubeDb.setVideos(mVideoRequest.categoryId, mContentListRecyclerAdapter.getData());
            isSaveReq = false;
        }
    }

    @Override
    public void onDestroy() {
        mAd.destroy(getContext());
        super.onDestroy();
    }

    @Override
    public void onItemClick(TubeItem item, final int position) {
        if (item.lockStatus == TubeItem.LOCK_STATUS_UNLOCKED || item.lockStatus == TubeItem.LOCK_STATUS_JUST_UNLOCKED) {

            Intent intent = new Intent(getActivity(), YouTubePlayerActivity.class);
            intent.putExtra(Constants.INTENT_KEY_VIDEO_ID, item.getStringId());
            intent.putExtra(Constants.INTENT_KEY_VIDEO_CATEGORY_ID, mVideoRequest.categoryId);
            intent.putExtra(Constants.INTENT_SUGGESTION_VIDEO_TYPE, mVideoRequest.suggestionVideoType);
            intent.putExtra(Constants.INTENT_KEY_CURRENT_ITEM, new Gson().toJson(item));
            isFreshCall = true; // So when we come back from back stack it will fetch videos it again
            saveVideos();

            startActivity(intent);
        } else {
            if (mAd.isLoaded()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("This video is locked");
                builder.setMessage("Watch a video ad to unlock this video.");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAd.show();
                        recentClickPosition = position;
                        //Show ads
                    }
                });
                builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
        }
    }

    @Override
    public void onActionClick(TubeItem item) {

    }

    @Override
    public void onItemRemoved(TubeItem item, int position) {

    }

    @Override
    protected int getType() {
        return ITEM_LARGE_ICON;
    }

    @Override
    protected void onInit(View view) {
        mAd = MobileAds.getRewardedVideoAdInstance(getContext());
        mAd.setRewardedVideoAdListener(this);

        //Setting Data
        Bundle bundle = getArguments();
        mVideoRequest = new VideoRequest();
        mVideoRequest.title = bundle.getString(TITLE);
        mVideoRequest.categoryId = bundle.getString(CATEGORY_ID);
        mVideoRequest.currentVideoId = bundle.getString(CURRENT_VIDEO_ID);
        mVideoRequest.suggestionVideoType = bundle.getInt(SUGGESTION_VIDEO_TYPE);
        mVideoRequest.hasNavigation = bundle.getBoolean(HAS_NAVIGATION);
        mVideoRequest.itemType = bundle.getInt(ITEM_TYPE);

        setSubtitle(mVideoRequest.title);

        if (mOnNavigationToggleListener != null) {
            mOnNavigationToggleListener.onNavigationChange(mVideoRequest.hasNavigation);
        }

        if (FirebaseRemoteConfig.getInstance().getBoolean(Constants.RemoteConfigConstant.IS_VIDEO_ADS_ENABLED)) {
            loadRewardedVideoAd();
        }

        getVideos();

    }

    private void loadRewardedVideoAd() {
        String videoAdId;
        if (BuildConfig.DEBUG) {
            videoAdId = FirebaseRemoteConfig.getInstance().getString(Constants.RemoteConfigConstant.AD_MOB_TEST_VIDEO_AD_ID);
        } else {
            videoAdId = FirebaseRemoteConfig.getInstance().getString(Constants.RemoteConfigConstant.AD_MOB_VIDEO_AD_ID);
        }
        mAd.loadAd(videoAdId, new AdRequest.Builder().build());
    }

    private void getVideos() {
        mProgressBar.setVisibility(View.VISIBLE);
        mErrorView.setVisibility(View.GONE);
        new TubeManager().getVideos(mVideoRequest, new OnOperationListener<List<TubeItem>>() {
            @Override
            public void onOperationListenerSuccess(List<TubeItem> response) {
                if (isAdded()) {
                    mContentListRecyclerAdapter.setData(response);
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onOperationListenerEmpty() {
                if (isAdded()) {
                    mErrorView.setRetryText("No videos found");
                    mProgressBar.setVisibility(View.GONE);
                    mErrorView.setVisibility(View.VISIBLE);
                    mErrorView.setOnClickListener(null);
                }
            }

            @Override
            public void onOperationListenerError(NetworkErrorThrowable t) {
                if (isAdded()) {
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    mProgressBar.setVisibility(View.GONE);
                    mErrorView.setSubtitle(t.getMessage());
                    mErrorView.setVisibility(View.VISIBLE);
                }
            }
        });

    }


    @Override
    protected void onRetryClick() {
        getVideos();
    }

    public void onRewardedVideoAdLoaded() {
        if (isAdded())
            Toast.makeText(getContext(), "Video Ads Available", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        if (isAdded()) {
            if (isAdWatched || BuildConfig.DEBUG) {
                isSaveReq = true;
                mContentListRecyclerAdapter.setLocked(TubeItem.LOCK_STATUS_JUST_UNLOCKED, recentClickPosition);
                isAdWatched = false;
            }
            loadRewardedVideoAd();
        }
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        if (isAdded()) {
            isAdWatched = true;
        }
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {

    }

}
