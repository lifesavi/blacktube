package com.apps.lifesavi.blacktube.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.apps.lifesavi.blacktube.application.BlackTubeApplication;
import com.apps.lifesavi.blacktube.constant.Constants;
import com.google.api.services.youtube.model.Channel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

public class TubePreference {
    public static SharedPreferences sharedPreferences;
    private static TubePreference tubePreference;

    public static TubePreference getInstance() {
        if (tubePreference == null) {
            tubePreference = new TubePreference();
            sharedPreferences = BlackTubeApplication.getInstance().getSharedPreferences("preference", Context.MODE_PRIVATE);
            sharedPreferences.getLong(Constants.PreferenceConstants.PREF_KEY_CURRENT_DATE,0);
        }
        return tubePreference;
    }

    public List<Channel> getChannelPreference() {
        return new Gson().fromJson(sharedPreferences.getString("channellist", ""), new TypeToken<List<Channel>>() {
        }.getType());
    }

    public void setChannelPreference(List<Channel> channelPreference) {
        String json = new Gson().toJson(channelPreference);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("channellist", json).apply();
    }

    public int getNetworkCallCount() {
        return sharedPreferences.getInt(Constants.PreferenceConstants.QUOTA_COST_COUNT, 0);
    }

    public void setNetworkCallCount(int count) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(Constants.PreferenceConstants.QUOTA_COST_COUNT, count);
        editor.apply();
    }

    public void incrementQuotaCountBy(int incrementBy) {
        setNetworkCallCount(getNetworkCallCount() + incrementBy);
    }

    public void clearPreference() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}

