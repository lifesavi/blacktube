package com.apps.lifesavi.blacktube.async;

import android.os.AsyncTask;

import com.apps.lifesavi.blacktube.application.BlackTubeApplication;
import com.apps.lifesavi.blacktube.exception.NetworkErrorThrowable;
import com.apps.lifesavi.blacktube.interfaces.OnOperationListener;
import com.apps.lifesavi.blacktube.model.BaseTubeResponse;
import com.apps.lifesavi.blacktube.model.ImageData;
import com.apps.lifesavi.blacktube.model.Thumbnails;
import com.apps.lifesavi.blacktube.model.TubeItem;
import com.apps.lifesavi.blacktube.operation.GetCategoryListOperation;
import com.apps.lifesavi.blacktube.utils.FileUtils;
import com.apps.lifesavi.blacktube.utils.TubeDb;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class GetCategoryAsyncTask extends AsyncTask<Void, Void, List<TubeItem>> implements OnOperationListener<BaseTubeResponse> {

    private OnOperationListener<List<TubeItem>> onOperationListener;


    public GetCategoryAsyncTask setOnOperationListener(OnOperationListener<List<TubeItem>> onOperationListener) {
        this.onOperationListener = onOperationListener;
        return this;
    }

    @Override
    protected List<TubeItem> doInBackground(Void... params) {
        return TubeDb.getCategories();
    }

    @Override
    protected void onPostExecute(List<TubeItem> tubeItems) {
        if (tubeItems != null && onOperationListener != null) {
            onOperationListener.onOperationListenerSuccess(tubeItems);
        } else {
            new GetCategoryListOperation(TubeDb.getSelectedRegionCode(), GetCategoryAsyncTask.this);
        }
    }

    @Override
    public void onOperationListenerSuccess(BaseTubeResponse response) {
        List<TubeItem> tubeItems = new ArrayList<>();

        List<ImageData> mImageDataList = FileUtils.getFromAssetsFolder(BlackTubeApplication.getContext(), "imagedata.json", null, new TypeToken<List<ImageData>>() {
        }.getType());


        if (response != null && response.getItems() != null) {
            for (TubeItem channel : response.getItems()) {
                if (channel != null && channel.getSnippet() != null && channel.getSnippet().isAssignable()) {
                    for (ImageData imageData : mImageDataList) {
                        if (imageData.getKeywords().contains(channel.getSnippet().getTitle())) {
                            if (channel.getSnippet().getThumbnails() == null) {
                                channel.getSnippet().setThumbnails(new Thumbnails());
                            }
                            channel.getSnippet().getThumbnails().setLocalDrawablePath(imageData.getDrawableImage());
                            break;
                        }
                    }
                    tubeItems.add(channel);
                }
            }
        }
        Collections.sort(tubeItems, new Comparator<TubeItem>() {
            @Override
            public int compare(TubeItem o1, TubeItem o2) {
                return o1.getSnippet().getTitle().compareToIgnoreCase(o2.getSnippet().getTitle());
            }
        });

        TubeDb.setCategories(tubeItems);

        if (onOperationListener != null) {
            onOperationListener.onOperationListenerSuccess(tubeItems);
        }

    }

    @Override
    public void onOperationListenerEmpty() {
        if (onOperationListener != null) {
            onOperationListener.onOperationListenerEmpty();
        }
    }

    @Override
    public void onOperationListenerError(NetworkErrorThrowable t) {
        if (onOperationListener != null) {
            onOperationListener.onOperationListenerError(t);
        }
    }
}
