package com.apps.lifesavi.blacktube.utils;


import com.apps.lifesavi.blacktube.model.Region;
import com.apps.lifesavi.blacktube.model.TubeItem;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_CONSISTENT;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_KEY_RECENT_REGIONS;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_KEY_REGIONS;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_KEY_REGION_CODE;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_KEY_SAVED_COUNT;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_KEY_SAVED_VIDEO;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_LABEL_CATEGORIES;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_LABEL_CURRENT_CHANNEL_ITEM;
import static com.apps.lifesavi.blacktube.constant.Constants.PaperDbConstants.PAPER_DB_LABEL_DEFAULT_REGION_CODE;

public class TubeDb {

    //Categories Based on Region
    public static void setCategories(List<TubeItem> categories) {
        Paper.book().write(getSelectedRegionCode() + PAPER_DB_LABEL_CATEGORIES, categories);
    }

    public static List<TubeItem> getCategories() {
        return Paper.book().read(getSelectedRegionCode() + PAPER_DB_LABEL_CATEGORIES, null);
    }

    //Region Code
    public static void setSelectedRegionTo(String region) {
        Paper.book(PAPER_DB_CONSISTENT).write(PAPER_DB_KEY_REGION_CODE, region);
    }

    public static String getSelectedRegionCode() {
        return Paper.book(PAPER_DB_CONSISTENT).read(PAPER_DB_KEY_REGION_CODE, PAPER_DB_LABEL_DEFAULT_REGION_CODE);
    }

    //Save Videos
    public static void setSaveVideos(List<TubeItem> tubeItemList) {
        Paper.book(PAPER_DB_CONSISTENT).write(PAPER_DB_KEY_SAVED_VIDEO, tubeItemList);
    }

    public static List<TubeItem> getSaveVideos() {
        return Paper.book(PAPER_DB_CONSISTENT).read(PAPER_DB_KEY_SAVED_VIDEO, new ArrayList<TubeItem>());
    }

    //Save Video Count
    public static void setSaveCount(int count) {
        Paper.book(PAPER_DB_CONSISTENT).write(PAPER_DB_KEY_SAVED_COUNT, count);
    }

    public static int getSaveCount() {
        return Paper.book(PAPER_DB_CONSISTENT).read(PAPER_DB_KEY_SAVED_COUNT, 0);
    }

    //Global Regions
    public static List<Region> getRegions() {
        return Paper.book().read(PAPER_DB_KEY_REGIONS, new ArrayList<Region>());
    }

    public static void setRegions(List<Region> regionList) {
        Paper.book().write(PAPER_DB_KEY_REGIONS, regionList);
    }

    //Local Saved Regions
    public static void setLocalRegions(List<Region> regionList) {
        Paper.book().write(PAPER_DB_KEY_RECENT_REGIONS, regionList);
    }

    public static List<Region> getLocalRegions() {
        return Paper.book().read(PAPER_DB_KEY_RECENT_REGIONS, new ArrayList<Region>());
    }

    //Videos based on Category
    public static void setVideos(String categoryId, List<TubeItem> videos) {
        Paper.book().write(AppUtils.getCategoryRegionKey(categoryId, getSelectedRegionCode()), videos);
    }

    public static List<TubeItem> getVideos(String categoryId) {
        return Paper.book().read(AppUtils.getCategoryRegionKey(categoryId, getSelectedRegionCode()), null);
    }

    //Current Item

    public static void setCurrentVideoItem(TubeItem tubeItem) {
        Paper.book().write(PAPER_DB_LABEL_CURRENT_CHANNEL_ITEM, tubeItem);
    }

    public static TubeItem getCurrentVideoItem() {
        return Paper.book().read(PAPER_DB_LABEL_CURRENT_CHANNEL_ITEM, null);
    }
}
